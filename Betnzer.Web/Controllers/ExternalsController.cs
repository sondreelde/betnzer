using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Betnzer.Web.HelperModels;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Betnzer.Services.Interfaces;

namespace Betnzer.Web.Controllers
{
    [ResponseCache(Duration = 60*60)]
    [Route("api/external")]
    [ApiController]
    public class ExternalsController : ControllerBase
    {

        [HttpGet("players")]
        public async Task<ActionResult> GetPlayers(){
            string baseUrl = "https://fantasy.premierleague.com/api/bootstrap-static/";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    // Send request
                    using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                    {
                        // Get results
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            JObject joResponse = JObject.Parse(data);

                            List<Player> playerList = new List<Player>();

                            // Format the data
                            foreach (var item in joResponse["elements"])
                            {
                                int id = (int)item["id"];
                                string name = (string)item["first_name"] + " " + (string)item["second_name"];
                                int teamId = (int)item["team_code"]; // TODO is this correct?
                                int scored = (int)item["goals_scored"];
                                string photo = (string)item["photo"];
                                int position = (int)item["element_type"];

                                playerList.Add(new Player(){ Id = id, Name = name, TeamId = teamId, Scored = scored, Photo = photo, Position = position });
                            }
                            
                            return Ok(playerList);
                        }
                    }
                }
            } catch(Exception exception)
            {
                return NotFound(exception);
            }
        }

        [HttpGet("teams")]
        public async Task<ActionResult> GetTeams(){
            string baseUrl = "https://fantasy.premierleague.com/api/bootstrap-static/";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    // Send request
                    using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                    {
                        // Get results
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            JObject joResponse = JObject.Parse(data);

                            List<Team> teams = new List<Team>();

                            foreach(var item in joResponse["teams"])
                            {
                                int id = (int)item["code"];
                                string name = (string)item["name"];
                                int placement = (int)item["strength"];

                                teams.Add(new Team(){ Id = id, Name = name, Placement = placement });
                            }

                            return Ok(teams);
                        }
                    }
                }
            } catch(Exception exception)
            {
                return NotFound(exception);
            }
        }

        [HttpGet("table")]
        public async Task<ActionResult> GetTable(){
            string baseUrl = "https://prod-public-api.livescore.com/v1/api/react/stage/soccer/england/premier-league/2.00";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    // Send request
                    using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                    {
                        // Get results
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            JObject joResponse = JObject.Parse(data);

                            List<Team> teams = new List<Team>();

                            foreach(var item in joResponse["Stages"][0]["LeagueTable"]["L"][0]["Tables"][0]["team"])
                            {
                                string name = (string)item["Tnm"];
                                int placement = (int)item["rnk"];
                                int points = (int)item["pts"];
                                int gd = (int)item["gd"];

                                teams.Add(new Team(){ Name = name, Placement = placement, GoalDifference = gd, Points = points });
                            }

                            return Ok(teams);
                        }
                    }
                }
            } catch(Exception exception)
            {
                return NotFound(exception);
            }
        }

        [HttpGet("games")]
        public async Task<ActionResult> GetGames(){
            string baseUrl = "https://fantasy.premierleague.com/api/bootstrap-static/";
            List<Gameweek> gameweeks = new List<Gameweek>();

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    // Send request
                    using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                    {
                        // Get results
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            JObject joResponse = JObject.Parse(data);


                            foreach(var item in joResponse["events"])
                            {
                                int id = (int)item["id"];
                                bool finished = (bool)item["finished"];
                                bool isNext = (bool)item["is_next"];

                                gameweeks.Add(new Gameweek(){ Id = id, Finished = finished, IsNext = isNext, Results = new List<Result>() });
                            }

                        }
                    }

                    // fill fixtures
                    baseUrl = "https://fantasy.premierleague.com/api/fixtures";
                    using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                    {
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            data = "{\"fixtures\":" + data + "}"; // Make the JSON parsable
                            JObject joResponse = JObject.Parse(data);

                            foreach(var item in joResponse.First.First)
                            {
                                int? gameweek = (int?)item["event"];

                                // uninterested if gameweek not assigned
                                if(gameweek == null){
                                    continue;
                                }

                                bool finished = (bool)item["finished"];
                                bool? started = (bool?)item["started"];
                                int home = (int)item["team_h"];
                                int away = (int)item["team_a"];

                                int? homescore = null;
                                int? awayscore = null;

                                List<int> scored = new List<int>();

                                if(finished){
                                    homescore = (int)item["team_h_score"];
                                    awayscore = (int)item["team_a_score"];

                                    var goalData = item["stats"][0];

                                    // add scorers from the home team
                                    foreach(var scorer in goalData["h"]){
                                        scored.Add((int)scorer["element"]);
                                    }
                                    // Add scorers from the awayteam
                                    foreach(var scorer in goalData["a"]){
                                        scored.Add((int)scorer["element"]);
                                    }  
                                }
                                
                                // add the fixture to its belonging gameweek
                                gameweeks.Find(gw => gw.Id == gameweek).Results.Add(
                                                        new Result(){ Gameweek = gameweek, Finished = finished, Started = started,
                                                            Home = home, Away = away, HomeScore = homescore, AwayScore = awayscore,
                                                            Scored = scored});
                            }
                        }
                    }
                    return Ok(gameweeks);
                }
            } catch(Exception exception)
            {
                return NotFound(exception);
            }
        }


        [HttpGet("draft")]
        public async Task<ActionResult> GetDraft(){
            string baseUrl = "https://draft.premierleague.com/api/league/102612/details";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    // Send request
                    using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                    {
                        // Get results
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            JObject joResponse = JObject.Parse(data);

                            List<Draft> drafts = new List<Draft>();

                            foreach (var item in joResponse["standings"])
                            {
                                int id = (int)item["league_entry"];
                                int roundpoints = (int)item["event_total"];
                                int points = (int)item["total"];
                                int place = (int)item["rank"];

                                var info = GetEntry(joResponse["league_entries"], id);
                                string teamname = (string)info["entry_name"];
                                string firstname = (string)info["player_first_name"];

                                drafts.Add(new Draft(){ Id = id, RoundPoints = roundpoints, Points = points,
                                                Place = place, TeamName = teamname, FirstName = firstname });                 
                            }

                            return Ok(drafts);
                        }
                    }
                }
            } catch(Exception exception)
            {
                return NotFound(exception);
            }
        }

        private JToken GetEntry(JToken jTokens, int id)
        {
            foreach (JToken item in jTokens)
            {
                if((int?)item["id"] == id){
                    return item;
                }
            }
            return null;
        }
    }
}