using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Betnzer.Data;
using Betnzer.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Betnzer.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GuesserController : ControllerBase
    {
        private readonly ApplicationDBContext _context;

        public GuesserController(ApplicationDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Guesser>>> GetGuessers()
        {
            return await _context.Guessers.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Guesser>> GetGuesser(int id)
        {
            var guesser = await _context.Guessers.FindAsync(id);

            if (guesser == null)
            {
                return NotFound();
            }

            guesser.SeasonGuesses = await _context.SeasonGuesses.Where(sg => sg.GuesserId == guesser.Id)
                                            .Include(sg => sg.MatchGuesses)
                                            .Include(sg => sg.Table)
                                            .Include(sg => sg.QuestionAnswer)
                                            .Include(sg => sg.GoalScorers)
                                            .Include(sg => sg.TopScorers)
                                            .ToListAsync();
            return guesser;

        }
    }
}