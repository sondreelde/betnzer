namespace Betnzer.Web.HelperModels
{
    class Draft
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public string FirstName { get; set; }
        public int Place { get; set; }
        public int RoundPoints { get; set; }
        public int Points { get; set; }
    }
}