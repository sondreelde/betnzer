namespace Betnzer.Web.HelperModels
{
    class Team
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int Placement { get; set; }
        public int Points { get; set; }
        public int GoalDifference { get; set; }
    }
}