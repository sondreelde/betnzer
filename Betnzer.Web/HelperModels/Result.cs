using System.Collections.Generic;

namespace Betnzer.Web.HelperModels
{
    class Result
    {
        public int? Gameweek { get; set; }
        public bool Finished { get; set; }  
        public bool? Started { get; set; }
        public int Home { get; set; }
        public int Away { get; set; }
        public int? HomeScore { get; set; }
        public int? AwayScore { get; set; }
        public List<int> Scored { get; set; }

    }
}