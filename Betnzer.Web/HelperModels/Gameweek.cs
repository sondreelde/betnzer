using System.Collections.Generic;

namespace Betnzer.Web.HelperModels
{
    class Gameweek
    {
        public int Id { get; set; }
        public bool Finished { get; set; }
        public bool IsNext { get; set; }
        public List<Result> Results { get; set; }
    }
}