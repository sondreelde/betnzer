namespace Betnzer.Web.HelperModels
{
    class Player
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int Scored { get; set; }
        public string Photo { get; set; }
        public int Position { get; set; }
    }
}