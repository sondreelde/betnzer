# Betnzer

Make the prediction competition of a football season more interactive. Submitted answers are displayed neatly and corrected for points.

## Getting Started

Example on how to start the project locally.

### Prerequisites

Requred programs to run the project locally.

Dotnet core 3.1

```
https://dotnet.microsoft.com/
```

Node.js
```
https://nodejs.org/en/
```

### Installing

Build the backend/proxy
```
dotnet build
```

Install node dependencies
```
cd betnzer-fontend && npm i
```

### Start project
Run the backend/proxy
```
dotnet run
```

Run frontend vue-app
```
cd betnzer-frontend && npm run serve
```


## Built With

* [Vuejs](https://vuejs.org/) - The web framework used
* [Nodejs](https://nodejs.org/en/) - Dependency Management (frontend)
* [Dotnet core](https://dotnet.microsoft.com/) - backend
* [Nuget](https://www.nuget.org/) - Dependency Management (backend)