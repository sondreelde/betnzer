import Vue from 'vue'
import VueRouter from 'vue-router'
import Next from '../views/Next.vue'
import Rank from '../views/Rank.vue'
import Guesses from '../views/Guesses.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Next',
    component: Next
  },
  {
    path: '/rank',
    name: 'Rank',
    component: Rank
  },
  {
    path: '/guesses',
    name: 'Guesses',
    component: Guesses
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
