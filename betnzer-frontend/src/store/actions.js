import Vue from "vue"

export const loadTeams = async ({ commit }) => {
    Vue.http.get("external/teams")
    .then(response => response.json())
    .then(data => commit("SET_TEAMS", data))
}

export const loadPlayers = async ({ commit }) => {
    Vue.http.get("external/players")
    .then(response => response.json())
    .then(data => commit("SET_PLAYERS", data))
}

export const loadGameweeks = async ({ commit }) => {
    Vue.http.get("external/games")
    .then(response => response.json())
    .then(data => commit("SET_GAMEWEEKS", data))
}

export const loadTable = async ({ commit }) => {
    Vue.http.get("external/table")
    .then(response => response.json())
    .then(data => commit("SET_TABLE", data))
}