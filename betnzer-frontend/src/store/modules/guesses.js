import { db } from "../../firebase"

const arrayToObject = arr => {
    let ret = {}
    for(const [key, value] of arr.entries()){
      if(key != 0){
        ret[key] = value
      }
    }
    return ret
  }

const state = {
    matches: {},
    meta: {},
    points: {},
    questions: {},
    rules: {},
    teams: {},
    guesses: {}
}

const getters = {
    gmatches: state => { return state.matches },
    gmeta: state => { return state.meta },
    gpoints: state => { return state.points },
    gquestions: state => { return state.questions },
    grules: state => { return state.rules },
    gteams: state => { return state.teams },
    guesses: state => { return state.guesses },
    gteamById: state => id => {
        return state.teams[id]
    }
}

const mutations = {
    SET_GUESSES(state, {matches, meta, points, questions, rules, teams, guess}) {
        state.matches = arrayToObject(matches)
        state.meta = meta
        state.points = points
        state.questions = arrayToObject(questions)
        state.rules = rules
        state.teams = arrayToObject(teams)
        state.guesses = guess
    }
}

const actions = {
    loadGuesses({commit}) {
        db.ref().on("value", snapshot => {
        commit("SET_GUESSES", snapshot.val())
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
