const isSameClub = (clubname, othername) => {
    const namearr1 = clubname.split(" ")
    const namearr2 = othername.split(" ")

    if(namearr1[0] != namearr2[0]){
        return false
    } else if (namearr1.length > 1 && namearr2.length > 1 ) {
        return namearr1[1] == namearr2[1]
    }
    return true
}

const state = {
    list: [],
    table: []
}

const mutations = {
    "SET_TEAMS"(state, data){
        state.list = data
    },
    "SET_TABLE"(state, data){
        state.table = data
    }
}

const getters = {
    // Get team base on id
    getTeam: state => id => {
        return state.list.find(team => team.id == id)
    },
    table: state => {
        return state.table
    },
    // Get team based on name
    getTeamByName: state => name => {
        return state.list.find(team => isSameClub(team.name, name))
    }
}

export default {
    mutations,
    getters,
    state
}