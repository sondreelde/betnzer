const state = {
    gameweeks: []
}

const mutations = {
    "SET_GAMEWEEKS"(state, data) {
        state.gameweeks = data
    }
}

const getters = {
    nextGameweek: state => {
        let last = {finished: true}
        for(const week of state.gameweeks){
            if(!week.finished && last.finished){
                return week
            }
            last = week
        }
        return 1
    },
    getGameweek: state => week => {
        return state.gameweeks.find(gameweek => gameweek.id == week)
    }
}

export default {
    state,
    mutations,
    getters
}