const similarName = (name1, name2) => {
    name1 = name1.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    name2 = name2.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    return name1.includes(name2) || name2.includes(name1)
}

const state = {
    list: [],
    baseSrc: "https://resources.premierleague.com/premierleague/photos/players/110x140/"
}

const mutations = {
    "SET_PLAYERS"(state, data){
        state.list = data
    }
}

const getters = {
    getBaseSrc: state => {
        return state.baseSrc
    },
    getPlayer: state => id => {
        return state.list.find(player => player.id == id)
    },
    topScorers: state => {
        let topScorers = state.list.sort((a,b) => b.scored - a.scored).slice(0,5)
        return topScorers
    },
    getPlayerByName: state => name => {
        let ret = state.list.find(player => similarName(player.name, name))
        if(ret !== undefined) return ret

        // search by last name instead
        name = name.slice(name.lastIndexOf(" ")) 
        ret = state.list.filter(player => similarName(player.name.slice(name.lastIndexOf(" ")), name))
        ret.sort((a,b) => {
            const aNameLength = a.name.slice(name.lastIndexOf(" ")).length
            const bNameLength = b.name.slice(name.lastIndexOf(" ")).length

            const aDiff = Math.abs(name.length - aNameLength)
            const bDiff = Math.abs(name.length - bNameLength)
            console.log(a.name, b.name, aDiff, bDiff)
            return aDiff - bDiff
        })
        return ret[0]
    }
}

export default {
    state,
    mutations,
    getters
}