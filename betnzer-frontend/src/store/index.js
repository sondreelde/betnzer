import Vue from 'vue'
import Vuex from 'vuex'

import players from "./modules/players"
import teams from "./modules/teams"
import gameweeks from "./modules/gameweeks"
import guesses from "./modules/guesses"
import * as actions from "./actions"

Vue.use(Vuex)

export default new Vuex.Store({
  actions,
  modules: {
    players,
    teams,
    gameweeks,
    guesses,
  }
})
