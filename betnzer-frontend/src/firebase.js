import { initializeApp } from "firebase/app"
import "firebase/database"

const app = initializeApp({
  apiKey: "AIzaSyCkPE7WInnLSazc_fD_am-2zj1aKzIeKrM",
  authDomain: "betnzer.firebaseapp.com",
  databaseURL: "https://betnzer.firebaseio.com",
  projectId: "betnzer",
  storageBucket: "betnzer.appspot.com",
  messagingSenderId: "327649158418",
  appId: "1:327649158418:web:aa1d247e22447a0a27bfcb",
  measurementId: "G-GW90LDW2GK"
})

export const db = app.database()
export const ref = db.ref("guess")
