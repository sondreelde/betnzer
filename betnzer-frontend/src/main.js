import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueResource from "vue-resource"
import './firebase'
import { firestorePlugin } from "vuefire"


Vue.config.productionTip = false

Vue.use(firestorePlugin)
Vue.use(VueResource)

Vue.http.options.root = process.env.VUE_APP_API_URL

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
