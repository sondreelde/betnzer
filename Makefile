PROJECT_NAME ?= Betnzer

.PHONY: migrations db

migrations: 
	cd ./Betnzer.Data && dotnet ef --startup-project ../Betnzer.Web/ migrations add $(name) && cd ..

db:
	cd ./Betnzer.Data && dotnet ef --startup-project ../Betnzer.Web/ database update && cd ..