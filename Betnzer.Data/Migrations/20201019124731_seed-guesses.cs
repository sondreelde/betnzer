﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Betnzer.Data.Migrations
{
    public partial class seedguesses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Guessers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guessers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Seasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameWeeks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WeekNumber = table.Column<int>(nullable: false),
                    SeasonId = table.Column<int>(nullable: false),
                    HomeTeam = table.Column<string>(nullable: true),
                    AwayTeam = table.Column<string>(nullable: true),
                    Result = table.Column<char>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameWeeks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GameWeeks_Seasons_SeasonId",
                        column: x => x.SeasonId,
                        principalTable: "Seasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Short = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    SeasonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questions_Seasons_SeasonId",
                        column: x => x.SeasonId,
                        principalTable: "Seasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SeasonGuesses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SeasonId = table.Column<int>(nullable: false),
                    GuesserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeasonGuesses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SeasonGuesses_Guessers_GuesserId",
                        column: x => x.GuesserId,
                        principalTable: "Guessers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeasonGuesses_Seasons_SeasonId",
                        column: x => x.SeasonId,
                        principalTable: "Seasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoalScorers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    SeasonGuessId = table.Column<int>(nullable: false),
                    GameWeekId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalScorers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoalScorers_GameWeeks_GameWeekId",
                        column: x => x.GameWeekId,
                        principalTable: "GameWeeks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoalScorers_SeasonGuesses_SeasonGuessId",
                        column: x => x.SeasonGuessId,
                        principalTable: "SeasonGuesses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchGuesses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Res = table.Column<char>(nullable: false),
                    GameWeekId = table.Column<int>(nullable: false),
                    SeasonGuessId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchGuesses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MatchGuesses_GameWeeks_GameWeekId",
                        column: x => x.GameWeekId,
                        principalTable: "GameWeeks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MatchGuesses_SeasonGuesses_SeasonGuessId",
                        column: x => x.SeasonGuessId,
                        principalTable: "SeasonGuesses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionAnswers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Ans = table.Column<string>(nullable: true),
                    QuestionId = table.Column<int>(nullable: false),
                    SeasonGuessId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionAnswers_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuestionAnswers_SeasonGuesses_SeasonGuessId",
                        column: x => x.SeasonGuessId,
                        principalTable: "SeasonGuesses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tables",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SeasonGuessId = table.Column<int>(nullable: false),
                    p1 = table.Column<string>(nullable: true),
                    p2 = table.Column<string>(nullable: true),
                    p3 = table.Column<string>(nullable: true),
                    p4 = table.Column<string>(nullable: true),
                    p5 = table.Column<string>(nullable: true),
                    p6 = table.Column<string>(nullable: true),
                    p7 = table.Column<string>(nullable: true),
                    p8 = table.Column<string>(nullable: true),
                    p9 = table.Column<string>(nullable: true),
                    p10 = table.Column<string>(nullable: true),
                    p11 = table.Column<string>(nullable: true),
                    p12 = table.Column<string>(nullable: true),
                    p13 = table.Column<string>(nullable: true),
                    p14 = table.Column<string>(nullable: true),
                    p15 = table.Column<string>(nullable: true),
                    p16 = table.Column<string>(nullable: true),
                    p17 = table.Column<string>(nullable: true),
                    p18 = table.Column<string>(nullable: true),
                    p19 = table.Column<string>(nullable: true),
                    p20 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tables_SeasonGuesses_SeasonGuessId",
                        column: x => x.SeasonGuessId,
                        principalTable: "SeasonGuesses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TopScorers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Scorer1 = table.Column<string>(nullable: true),
                    Goals1 = table.Column<int>(nullable: false),
                    Scorer2 = table.Column<string>(nullable: true),
                    Goals2 = table.Column<int>(nullable: false),
                    Scorer3 = table.Column<string>(nullable: true),
                    Goals3 = table.Column<int>(nullable: false),
                    Scorer4 = table.Column<string>(nullable: true),
                    Goals4 = table.Column<int>(nullable: false),
                    Scorer5 = table.Column<string>(nullable: true),
                    Goals5 = table.Column<int>(nullable: false),
                    SeasonGuessId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopScorers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TopScorers_SeasonGuesses_SeasonGuessId",
                        column: x => x.SeasonGuessId,
                        principalTable: "SeasonGuesses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Guessers",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Sondre" },
                    { 2, "Øystein" },
                    { 3, "Ivar" },
                    { 4, "Makke" },
                    { 5, "Marty" },
                    { 6, "Benny" }
                });

            migrationBuilder.InsertData(
                table: "Seasons",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "20/21" });

            migrationBuilder.InsertData(
                table: "GameWeeks",
                columns: new[] { "Id", "AwayTeam", "HomeTeam", "Result", "SeasonId", "WeekNumber" },
                values: new object[,]
                {
                    { 1, "16", "6", 'h', 1, 1 },
                    { 22, "5", "17", 'x', 1, 22 },
                    { 23, "12", "11", 'x', 1, 23 },
                    { 24, "2", "3", 'x', 1, 24 },
                    { 25, "6", "3", 'x', 1, 25 },
                    { 26, "1", "10", 'x', 1, 26 },
                    { 27, "9", "19", 'x', 1, 27 },
                    { 28, "17", "1", 'x', 1, 28 },
                    { 21, "13", "1", 'x', 1, 21 },
                    { 29, "1", "19", 'x', 1, 29 },
                    { 32, "17", "7", 'x', 1, 32 },
                    { 33, "4", "20", 'x', 1, 33 },
                    { 34, "10", "3", 'x', 1, 34 },
                    { 35, "7", "19", 'x', 1, 35 },
                    { 36, "15", "7", 'x', 1, 36 },
                    { 37, "9", "18", 'x', 1, 37 },
                    { 38, "17", "10", 'x', 1, 38 },
                    { 30, "15", "9", 'x', 1, 30 },
                    { 20, "10", "7", 'x', 1, 20 },
                    { 31, "13", "4", 'x', 1, 31 },
                    { 18, "7", "20", 'x', 1, 18 },
                    { 19, "17", "15", 'x', 1, 19 },
                    { 2, "11", "5", 'b', 1, 2 },
                    { 3, "20", "19", 'h', 1, 3 },
                    { 4, "17", "13", 'b', 1, 4 },
                    { 5, "3", "6", 'x', 1, 5 },
                    { 7, "1", "13", 'x', 1, 7 },
                    { 8, "11", "12", 'x', 1, 8 },
                    { 9, "3", "2", 'x', 1, 9 },
                    { 6, "5", "13", 'x', 1, 6 },
                    { 11, "1", "17", 'x', 1, 11 },
                    { 12, "12", "13", 'x', 1, 12 },
                    { 13, "14", "9", 'x', 1, 13 },
                    { 14, "20", "4", 'x', 1, 14 },
                    { 15, "5", "1", 'x', 1, 15 },
                    { 16, "9", "18", 'x', 1, 16 },
                    { 10, "17", "5", 'x', 1, 10 },
                    { 17, "12", "5", 'x', 1, 17 }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "SeasonId", "Short", "Text" },
                values: new object[,]
                {
                    { 15, 1, "FA-cupen", "Hvilket lag vinner FA-cup finalen?" },
                    { 8, 1, "Boxing goals", "[Over/Under] 19.5 blir scoret på boxing day?" },
                    { 9, 1, "Leeds solid?", "Leeds slipper inn 5 eller flere mål i en kamp?" },
                    { 10, 1, "Leicester galore?", "Leicester scorer 5 eller flere mål i en kamp?" },
                    { 14, 1, "Flest mål v.2", "Hvem scorer flest mål av [ Iheanacho / Wesley / Yarmolenko ]?" },
                    { 12, 1, "Avgjort", "I hvilken runde blir Premier League trofeét sikret? (FPL-runde)" },
                    { 13, 1, "Flest mål", "Hvem scorer flest mål av [ Barnes / Trezeguet / McBurnie ]?" },
                    { 7, 1, "One hit wonder", "Nevn én spiller som scorer nøyaktig ett mål." },
                    { 11, 1, "Clean sheets", "Hvilken keeper får flest clean sheets?" },
                    { 6, 1, "Assist", "Hvilken spiller får flest assist? Vunnet straffe telles ikke. (OPTA)" },
                    { 17, 1, "PFA", "Hvilken spiller blir kåret til PFA player of the year?" },
                    { 4, 1, "Play like cunts", "Hvilket lag får flest gule kort?" },
                    { 3, 1, "Gult kort!", "Hvilken spiller får flest gule kort?" },
                    { 2, 1, "Sacked, when?", "I hvilken måned forlater den første manageren sin klubb?" },
                    { 1, 1, "Sacked", "Første manager til å forlate sin klubb?" },
                    { 16, 1, "Ligacupen", "Hvilket lag vinner ligacupen?" },
                    { 5, 1, "Røde kort(lag)", "Hvilket lag får flest røde kort?" }
                });

            migrationBuilder.InsertData(
                table: "SeasonGuesses",
                columns: new[] { "Id", "GuesserId", "SeasonId" },
                values: new object[,]
                {
                    { 6, 6, 1 },
                    { 5, 5, 1 },
                    { 4, 4, 1 },
                    { 2, 2, 1 },
                    { 1, 1, 1 },
                    { 3, 3, 1 }
                });

            migrationBuilder.InsertData(
                table: "GoalScorers",
                columns: new[] { "Id", "GameWeekId", "Name", "SeasonGuessId" },
                values: new object[,]
                {
                    { 11, 1, "Roberto Firmino", 1 },
                    { 67, 7, "Mane", 6 },
                    { 66, 6, "Salah", 6 },
                    { 65, 5, "Kane", 6 },
                    { 64, 4, "James Rodriguez", 6 },
                    { 63, 3, "Son", 6 },
                    { 62, 2, "Aubameyang", 6 },
                    { 61, 1, "Werner", 6 },
                    { 433, 33, "James Rodriguez", 4 },
                    { 432, 32, "Richarlison ", 4 },
                    { 431, 31, "Robertson", 4 },
                    { 430, 30, "Alexander Arnold", 4 },
                    { 68, 8, "Havertz", 6 },
                    { 31, 1, "Aubameyang", 3 },
                    { 33, 3, "Kane", 3 },
                    { 34, 4, "Werner", 3 },
                    { 35, 5, "Vardy", 3 },
                    { 36, 6, "Mane", 3 },
                    { 37, 7, "Salah", 3 },
                    { 38, 8, "Son Heung-min", 3 },
                    { 39, 9, "Martial", 3 },
                    { 310, 10, "De Bruyne", 3 },
                    { 311, 11, "Bruno Fernandes", 3 },
                    { 312, 12, "Jimenez", 3 },
                    { 313, 13, "Greenwood", 3 },
                    { 32, 2, "Rashford", 3 },
                    { 69, 9, "Rashford", 6 },
                    { 610, 10, "Vardy", 6 },
                    { 611, 11, "De Bruyne", 6 },
                    { 636, 36, "Ings", 6 },
                    { 635, 35, "Barnes", 6 },
                    { 634, 34, "Pulisic", 6 },
                    { 633, 33, "Mahrez", 6 },
                    { 632, 32, "Pogba", 6 },
                    { 631, 31, "Torres på city", 6 },
                    { 630, 30, "Mount", 6 },
                    { 629, 29, "Mitrovic", 6 },
                    { 628, 28, "Calvert-Levin", 6 },
                    { 627, 27, "Alexander Arnold", 6 },
                    { 626, 26, "michy batshuayi", 6 },
                    { 625, 25, "Greenwood", 6 },
                    { 624, 24, "Zieych", 6 },
                    { 623, 23, "Bergwjin", 6 },
                    { 622, 22, "Firmino", 6 },
                    { 621, 21, "Aguero", 6 },
                    { 620, 20, "Zaha", 6 },
                    { 619, 19, "Rodrigo", 6 },
                    { 618, 18, "Gabriel Jesus", 6 },
                    { 617, 17, "Martial", 6 },
                    { 616, 16, "Wood", 6 },
                    { 615, 15, "Grealish", 6 },
                    { 614, 14, "Bruno Fernandes", 6 },
                    { 613, 13, "Sterling", 6 },
                    { 612, 12, "Willian", 6 },
                    { 314, 14, "Mahrez", 3 },
                    { 315, 15, "Sterling", 3 },
                    { 316, 16, "Firmino", 3 },
                    { 317, 17, "Calvert Lewin", 3 },
                    { 421, 21, "Mings", 4 },
                    { 420, 20, "Bergwijn", 4 },
                    { 419, 19, "Ings", 4 },
                    { 418, 18, "Henderson", 4 },
                    { 417, 17, "Van Dijk", 4 },
                    { 416, 16, "Hueng min Son", 4 },
                    { 415, 15, "Greenwood", 4 },
                    { 414, 14, "Firminio", 4 },
                    { 413, 13, "Zieich ", 4 },
                    { 412, 12, "Jesus", 4 },
                    { 411, 11, "Aguero", 4 },
                    { 410, 10, "Woods", 4 },
                    { 49, 9, "Mane", 4 },
                    { 48, 8, "Vardy", 4 },
                    { 47, 7, "Martial", 4 },
                    { 46, 6, "Werner", 4 },
                    { 45, 5, "Kane", 4 },
                    { 44, 4, "Sterling", 4 },
                    { 43, 3, "Rashford", 4 },
                    { 42, 2, "Aubameyang", 4 },
                    { 41, 1, "Salah", 4 },
                    { 533, 33, "Grealish", 5 },
                    { 534, 34, "Havertz", 5 },
                    { 535, 35, "Nketiah", 5 },
                    { 536, 36, "Che Adams", 5 },
                    { 422, 22, "Mahrez", 4 },
                    { 238, 38, "Patrick Bamford", 2 },
                    { 423, 23, "Kevin De Bruyne", 4 },
                    { 425, 25, "Bruno Fernandes", 4 },
                    { 318, 18, "Dele Alli", 3 },
                    { 319, 19, "Bergwijn", 3 },
                    { 320, 20, "Aguero", 3 },
                    { 321, 21, "Willian", 3 },
                    { 322, 22, "Barnes", 3 },
                    { 323, 23, "Saka", 3 },
                    { 324, 24, "Pogba", 3 },
                    { 325, 25, "Van de Beek", 3 },
                    { 326, 26, "Callum Wilson", 3 },
                    { 327, 27, "Ings", 3 },
                    { 328, 28, "Richarlison", 3 },
                    { 329, 29, "Mitrovic", 3 },
                    { 330, 30, "Batshuayi", 3 },
                    { 331, 31, "Naby Keita", 3 },
                    { 332, 32, "Winjaldum", 3 },
                    { 333, 33, "Gabriel jesus", 3 },
                    { 334, 34, "James Rodriguez", 3 },
                    { 335, 35, "Maddison", 3 },
                    { 336, 36, "Adama Traore", 3 },
                    { 337, 37, "Havertz", 3 },
                    { 338, 38, "Chris Wood", 3 },
                    { 429, 29, "Saka", 4 },
                    { 428, 28, "Nicolas Pepe", 4 },
                    { 427, 27, "Willian", 4 },
                    { 426, 26, "Pulisic", 4 },
                    { 424, 24, "Pogba", 4 },
                    { 537, 37, "Greenwood", 5 },
                    { 237, 37, "Marcus Rashford", 2 },
                    { 235, 35, "Nicolas Pepe", 2 },
                    { 131, 31, "Che Adams", 1 },
                    { 132, 32, "Raheem Sterling", 1 },
                    { 133, 33, "Jack Grealish", 1 },
                    { 134, 34, "Hakim Ziyech", 1 },
                    { 135, 35, "Jay Rodriguez", 1 },
                    { 136, 36, "Kevin De Bruyne", 1 },
                    { 137, 37, "Anthony Martial", 1 },
                    { 138, 38, "Rodrigo Moreno Machado", 1 },
                    { 51, 1, "Salah", 5 },
                    { 52, 2, "Calvert Lewin", 5 },
                    { 53, 3, "Kane", 5 },
                    { 130, 30, "Timo Werner", 1 },
                    { 54, 4, "Raúl Jiménez", 5 },
                    { 56, 6, "Sterling", 5 },
                    { 57, 7, "Heung Min Son", 5 },
                    { 58, 8, "Pepe", 5 },
                    { 59, 9, "Martial", 5 },
                    { 510, 10, "Perez", 5 },
                    { 511, 11, "Werner", 5 },
                    { 512, 12, "Barnes", 5 },
                    { 513, 13, "Kevin de bruyne", 5 },
                    { 514, 14, "Pulisic", 5 },
                    { 515, 15, "Trezeguet", 5 },
                    { 516, 16, "Alli", 5 },
                    { 55, 5, "Vardy", 5 },
                    { 129, 29, "Aleksandar Mitrović", 1 },
                    { 128, 28, "Jordan Ayew", 1 },
                    { 127, 27, "Mohamed Salah", 1 },
                    { 12, 2, "Richarlison", 1 },
                    { 13, 3, "Mason Mount", 1 },
                    { 14, 4, "Raúl Jiménez", 1 },
                    { 15, 5, "Chris Wood", 1 },
                    { 16, 6, "Neal Maupay", 1 },
                    { 17, 7, "Ayoze Pérez", 1 },
                    { 18, 8, "Dele Alli", 1 },
                    { 19, 9, "Nicolas Pépé", 1 },
                    { 110, 10, "Jamie Vardy", 1 },
                    { 111, 11, "Christian Pulisic", 1 },
                    { 112, 12, "Michail Antonio", 1 },
                    { 113, 13, "Phil Foden", 1 },
                    { 114, 14, "Mason Greenwood", 1 },
                    { 115, 15, "Danny Ings", 1 },
                    { 116, 16, "Steven Bergwijn", 1 },
                    { 117, 17, "Alexandre Lacazette", 1 },
                    { 118, 18, "Sébastien Haller", 1 },
                    { 119, 19, "Diogo Jota", 1 },
                    { 120, 20, "Sergio Agüero", 1 },
                    { 121, 21, "Sadio Mané", 1 },
                    { 122, 22, "Dominic Calvert-Lewin", 1 },
                    { 123, 23, "Harry Kane", 1 },
                    { 124, 24, "Marcus Rashford", 1 },
                    { 125, 25, "Son Heung-Min", 1 },
                    { 126, 26, "Leandro Trossard", 1 },
                    { 517, 17, "Willian", 5 },
                    { 236, 36, "Paul Pogba", 2 },
                    { 518, 18, "Mahrez", 5 },
                    { 520, 20, "Jesus", 5 },
                    { 210, 10, "Sergio Aguero", 2 },
                    { 211, 11, "Raheem Sterling", 2 },
                    { 212, 12, "Willian", 2 },
                    { 213, 13, "Gabriel Jesus", 2 },
                    { 214, 14, "Hakim Ziyech", 2 },
                    { 215, 15, "Kevin de Bruyne", 2 },
                    { 216, 16, "Danny Ings", 2 },
                    { 217, 17, "Chris Wood", 2 },
                    { 218, 18, "Mohamed Salah", 2 },
                    { 219, 19, "Jamie Vardy", 2 },
                    { 220, 20, "Mason Greenwood", 2 },
                    { 221, 21, "James Rodriguez", 2 },
                    { 222, 22, "Oli McBurnie", 2 },
                    { 223, 23, "Steven Bergwijn", 2 },
                    { 224, 24, "Olivier Giroud", 2 },
                    { 225, 25, "Divock Origi", 2 },
                    { 226, 26, "Wilfried Zaha", 2 },
                    { 227, 27, "Alex Oxlade-Chamberlain", 2 },
                    { 228, 28, "Ayoze Perez", 2 },
                    { 229, 29, "Virgil Van Dijk", 2 },
                    { 230, 30, "Dominic Calvert-Lewin", 2 },
                    { 231, 31, "Kai Havertz", 2 },
                    { 232, 32, "Pierre-Emerick Aubameyang", 2 },
                    { 233, 33, "Rodrigo", 2 },
                    { 234, 34, "Thiago Silva", 2 },
                    { 29, 9, "Adama Traore", 2 },
                    { 519, 19, "Jota", 5 },
                    { 28, 8, "Sebastien Haller", 2 },
                    { 26, 6, "Roberto Firmino", 2 },
                    { 521, 21, "Bergwijn", 5 },
                    { 522, 22, "Mane", 5 },
                    { 523, 23, "Moura", 5 },
                    { 524, 24, "Zyiech", 5 },
                    { 525, 25, "Rashford", 5 },
                    { 526, 26, "Bernardo Silva", 5 },
                    { 527, 27, "Pogba", 5 },
                    { 528, 28, "Ings", 5 },
                    { 529, 29, "Rodrigo", 5 },
                    { 530, 30, "Bruno Fernandes", 5 },
                    { 531, 31, "Aguero", 5 },
                    { 532, 32, "Iheanacho", 5 },
                    { 438, 38, "Kante", 4 },
                    { 437, 37, "Bernardo Silva", 4 },
                    { 436, 36, "Dele Alli", 4 },
                    { 435, 35, "Laporte", 4 },
                    { 434, 34, "Rodrigo", 4 },
                    { 638, 38, "Jimenez", 6 },
                    { 637, 37, "Richarlison", 6 },
                    { 21, 1, "Sadio Mane", 2 },
                    { 22, 2, "Anthony Martial", 2 },
                    { 23, 3, "Heung-Min Son", 2 },
                    { 24, 4, "Timo Werner", 2 },
                    { 25, 5, "Harry Kane", 2 },
                    { 27, 7, "Raul Jimenez", 2 },
                    { 538, 38, "Callum Wilson", 5 }
                });

            migrationBuilder.InsertData(
                table: "MatchGuesses",
                columns: new[] { "Id", "GameWeekId", "Res", "SeasonGuessId" },
                values: new object[,]
                {
                    { 438, 38, 'h', 4 },
                    { 537, 37, 'b', 5 },
                    { 52, 2, 'b', 5 },
                    { 618, 18, 'h', 6 },
                    { 617, 17, 'h', 6 },
                    { 616, 16, 'u', 6 },
                    { 615, 15, 'b', 6 },
                    { 614, 14, 'u', 6 },
                    { 613, 13, 'h', 6 },
                    { 612, 12, 'b', 6 },
                    { 611, 11, 'h', 6 },
                    { 610, 10, 'h', 6 },
                    { 69, 9, 'h', 6 },
                    { 68, 8, 'h', 6 },
                    { 67, 7, 'h', 6 },
                    { 66, 6, 'b', 6 },
                    { 65, 5, 'h', 6 },
                    { 64, 4, 'u', 6 },
                    { 619, 19, 'u', 6 },
                    { 620, 20, 'h', 6 },
                    { 621, 21, 'h', 6 },
                    { 622, 22, 'b', 6 },
                    { 638, 38, 'b', 6 },
                    { 637, 37, 'h', 6 },
                    { 636, 36, 'h', 6 },
                    { 635, 35, 'b', 6 },
                    { 634, 34, 'b', 6 },
                    { 633, 33, 'h', 6 },
                    { 632, 32, 'h', 6 },
                    { 63, 3, 'u', 6 },
                    { 631, 31, 'b', 6 },
                    { 629, 29, 'b', 6 },
                    { 628, 28, 'u', 6 },
                    { 627, 27, 'h', 6 },
                    { 626, 26, 'h', 6 },
                    { 625, 25, 'u', 6 },
                    { 624, 24, 'h', 6 },
                    { 623, 23, 'u', 6 },
                    { 630, 30, 'h', 6 },
                    { 51, 1, 'b', 5 },
                    { 62, 2, 'h', 6 },
                    { 538, 38, 'b', 5 },
                    { 517, 17, 'b', 5 },
                    { 516, 16, 'b', 5 },
                    { 515, 15, 'b', 5 },
                    { 514, 14, 'u', 5 },
                    { 513, 13, 'h', 5 },
                    { 512, 12, 'u', 5 },
                    { 511, 11, 'h', 5 },
                    { 510, 10, 'h', 5 },
                    { 59, 9, 'u', 5 },
                    { 58, 8, 'b', 5 },
                    { 57, 7, 'h', 5 },
                    { 56, 6, 'h', 5 },
                    { 55, 5, 'h', 5 },
                    { 54, 4, 'h', 5 },
                    { 53, 3, 'b', 5 },
                    { 518, 18, 'h', 5 },
                    { 519, 19, 'b', 5 },
                    { 520, 20, 'b', 5 },
                    { 521, 21, 'u', 5 },
                    { 437, 37, 'u', 4 },
                    { 536, 36, 'h', 5 },
                    { 535, 35, 'b', 5 },
                    { 534, 34, 'b', 5 },
                    { 533, 33, 'h', 5 },
                    { 532, 32, 'b', 5 },
                    { 531, 31, 'b', 5 },
                    { 61, 1, 'h', 6 },
                    { 530, 30, 'h', 5 },
                    { 528, 28, 'u', 5 },
                    { 527, 27, 'b', 5 },
                    { 526, 26, 'b', 5 },
                    { 525, 25, 'b', 5 },
                    { 524, 24, 'h', 5 },
                    { 523, 23, 'h', 5 },
                    { 522, 22, 'b', 5 },
                    { 529, 29, 'b', 5 },
                    { 436, 36, 'u', 4 },
                    { 413, 13, 'h', 4 },
                    { 434, 34, 'b', 4 },
                    { 216, 16, 'b', 2 },
                    { 215, 15, 'h', 2 },
                    { 214, 14, 'b', 2 },
                    { 213, 13, 'h', 2 },
                    { 212, 12, 'b', 2 },
                    { 211, 11, 'u', 2 },
                    { 210, 10, 'h', 2 },
                    { 29, 9, 'b', 2 },
                    { 28, 8, 'u', 2 },
                    { 27, 7, 'h', 2 },
                    { 26, 6, 'u', 2 },
                    { 25, 5, 'h', 2 },
                    { 24, 4, 'h', 2 },
                    { 23, 3, 'b', 2 },
                    { 22, 2, 'b', 2 },
                    { 217, 17, 'h', 2 },
                    { 218, 18, 'h', 2 },
                    { 219, 19, 'b', 2 },
                    { 220, 20, 'h', 2 },
                    { 236, 36, 'h', 2 },
                    { 235, 35, 'b', 2 },
                    { 234, 34, 'b', 2 },
                    { 233, 33, 'h', 2 },
                    { 232, 32, 'b', 2 },
                    { 231, 31, 'b', 2 },
                    { 230, 30, 'h', 2 },
                    { 21, 1, 'b', 2 },
                    { 229, 29, 'b', 2 },
                    { 227, 27, 'b', 2 },
                    { 226, 26, 'b', 2 },
                    { 225, 25, 'h', 2 },
                    { 224, 24, 'h', 2 },
                    { 223, 23, 'u', 2 },
                    { 222, 22, 'b', 2 },
                    { 221, 21, 'h', 2 },
                    { 228, 28, 'h', 2 },
                    { 237, 37, 'h', 2 },
                    { 138, 38, 'b', 1 },
                    { 136, 36, 'h', 1 },
                    { 115, 15, 'b', 1 },
                    { 114, 14, 'b', 1 },
                    { 113, 13, 'b', 1 },
                    { 112, 12, 'u', 1 },
                    { 111, 11, 'b', 1 },
                    { 110, 10, 'h', 1 },
                    { 19, 9, 'u', 1 },
                    { 18, 8, 'h', 1 },
                    { 17, 7, 'h', 1 },
                    { 16, 6, 'h', 1 },
                    { 15, 5, 'u', 1 },
                    { 14, 4, 'h', 1 },
                    { 13, 3, 'b', 1 },
                    { 12, 2, 'b', 1 },
                    { 11, 1, 'b', 1 },
                    { 116, 16, 'b', 1 },
                    { 117, 17, 'b', 1 },
                    { 118, 18, 'b', 1 },
                    { 119, 19, 'b', 1 },
                    { 135, 35, 'b', 1 },
                    { 134, 34, 'b', 1 },
                    { 133, 33, 'h', 1 },
                    { 132, 32, 'u', 1 },
                    { 131, 31, 'b', 1 },
                    { 130, 30, 'b', 1 },
                    { 129, 29, 'b', 1 },
                    { 137, 37, 'b', 1 },
                    { 435, 35, 'u', 4 },
                    { 126, 26, 'b', 1 },
                    { 125, 25, 'u', 1 },
                    { 124, 24, 'u', 1 },
                    { 123, 23, 'b', 1 },
                    { 122, 22, 'b', 1 },
                    { 121, 21, 'b', 1 },
                    { 120, 20, 'u', 1 },
                    { 127, 27, 'h', 1 },
                    { 238, 38, 'b', 2 },
                    { 128, 28, 'h', 1 },
                    { 418, 18, 'h', 4 },
                    { 412, 12, 'b', 4 },
                    { 411, 11, 'b', 4 },
                    { 410, 10, 'h', 4 },
                    { 49, 9, 'u', 4 },
                    { 48, 8, 'h', 4 },
                    { 47, 7, 'h', 4 },
                    { 414, 14, 'b', 4 },
                    { 46, 6, 'b', 4 },
                    { 43, 3, 'b', 4 },
                    { 42, 2, 'u', 4 },
                    { 41, 1, 'h', 4 },
                    { 338, 38, 'b', 3 },
                    { 337, 37, 'b', 3 },
                    { 336, 36, 'h', 3 },
                    { 45, 5, 'h', 4 },
                    { 415, 15, 'b', 4 },
                    { 416, 16, 'u', 4 },
                    { 417, 17, 'b', 4 },
                    { 433, 33, 'h', 4 },
                    { 432, 32, 'b', 4 },
                    { 431, 31, 'b', 4 },
                    { 430, 30, 'h', 4 },
                    { 429, 29, 'b', 4 },
                    { 428, 28, 'h', 4 },
                    { 427, 27, 'b', 4 },
                    { 426, 26, 'h', 4 },
                    { 425, 25, 'b', 4 },
                    { 424, 24, 'u', 4 },
                    { 423, 23, 'h', 4 },
                    { 422, 22, 'b', 4 },
                    { 421, 21, 'h', 4 },
                    { 420, 20, 'b', 4 },
                    { 419, 19, 'h', 4 },
                    { 335, 35, 'b', 3 },
                    { 334, 34, 'b', 3 },
                    { 44, 4, 'h', 4 },
                    { 332, 32, 'b', 3 },
                    { 316, 16, 'b', 3 },
                    { 315, 15, 'u', 3 },
                    { 314, 14, 'u', 3 },
                    { 313, 13, 'h', 3 },
                    { 312, 12, 'b', 3 },
                    { 311, 11, 'b', 3 },
                    { 333, 33, 'h', 3 },
                    { 317, 17, 'b', 3 },
                    { 39, 9, 'b', 3 },
                    { 37, 7, 'h', 3 },
                    { 36, 6, 'h', 3 },
                    { 35, 5, 'u', 3 },
                    { 34, 4, 'h', 3 },
                    { 33, 3, 'h', 3 },
                    { 32, 2, 'b', 3 },
                    { 31, 1, 'h', 3 },
                    { 38, 8, 'h', 3 },
                    { 318, 18, 'h', 3 },
                    { 310, 10, 'h', 3 },
                    { 331, 31, 'b', 3 },
                    { 319, 19, 'b', 3 },
                    { 329, 29, 'b', 3 },
                    { 328, 28, 'u', 3 },
                    { 327, 27, 'b', 3 },
                    { 326, 26, 'b', 3 },
                    { 325, 25, 'h', 3 },
                    { 330, 30, 'u', 3 },
                    { 323, 23, 'h', 3 },
                    { 322, 22, 'h', 3 },
                    { 321, 21, 'b', 3 },
                    { 320, 20, 'h', 3 },
                    { 324, 24, 'b', 3 }
                });

            migrationBuilder.InsertData(
                table: "QuestionAnswers",
                columns: new[] { "Id", "Ans", "QuestionId", "SeasonGuessId" },
                values: new object[,]
                {
                    { 611, "Ederson", 11, 6 },
                    { 311, "Allison", 11, 3 },
                    { 411, "Allison", 11, 4 },
                    { 511, "De Gea", 11, 5 },
                    { 69, "Ja", 9, 6 },
                    { 412, "34", 12, 4 },
                    { 212, "35", 12, 2 },
                    { 312, "37", 12, 3 },
                    { 211, "Ederson", 11, 2 },
                    { 512, "38", 12, 5 },
                    { 612, "35", 12, 6 },
                    { 112, "37", 12, 1 },
                    { 111, "Ederson", 11, 1 },
                    { 68, "Over", 8, 6 },
                    { 510, "Nei", 10, 5 },
                    { 410, "Nei", 10, 4 },
                    { 310, "Ja", 10, 3 },
                    { 210, "Nei", 10, 2 },
                    { 19, "Ja", 9, 1 },
                    { 29, "Ja", 9, 2 },
                    { 39, "Nei", 9, 3 },
                    { 49, "Nei", 9, 4 },
                    { 59, "Nei", 9, 5 },
                    { 113, "Trezeguet", 13, 1 },
                    { 110, "Nei", 10, 1 },
                    { 610, "Nei", 10, 6 },
                    { 213, "McBurnie", 13, 2 },
                    { 42, "Oktober", 2, 4 },
                    { 413, "Barnes", 13, 4 },
                    { 417, "Kevin De Bruyne", 17, 4 },
                    { 317, "Aubameyang", 17, 3 },
                    { 217, "Kevin De Bruyne", 17, 2 },
                    { 117, "Bruno Fernandes", 17, 1 },
                    { 616, "Manchester City", 16, 6 },
                    { 516, "Manchester City", 16, 5 },
                    { 416, "Liverpool", 16, 4 },
                    { 58, "Over", 8, 5 },
                    { 316, "Tottenham", 16, 3 },
                    { 216, "Manchester City", 16, 2 },
                    { 116, "Manchester City", 16, 1 },
                    { 615, "Chelsea", 15, 6 },
                    { 515, "Manchester City", 15, 5 },
                    { 415, "Manchester City", 15, 4 },
                    { 315, "Chelsea", 15, 3 },
                    { 215, "Chelsea", 15, 2 },
                    { 115, "Manchester City", 15, 1 },
                    { 614, "Iheanacho", 14, 6 },
                    { 514, "Iheanacho", 14, 5 },
                    { 414, "Iheanacho", 14, 4 },
                    { 314, "Wesley", 14, 3 },
                    { 214, "Yarmolenko", 14, 2 },
                    { 114, "Wesley", 14, 1 },
                    { 613, "Barnes", 13, 6 },
                    { 513, "Barnes", 13, 5 },
                    { 313, "Barnes", 13, 3 },
                    { 48, "Over", 8, 4 },
                    { 617, "Kevin De Bruyne", 17, 6 },
                    { 28, "Over", 8, 2 },
                    { 14, "Arsenal", 4, 1 },
                    { 63, "Westwood", 3, 6 },
                    { 53, "Luka Milivojevic", 3, 5 },
                    { 38, "Over", 8, 3 },
                    { 33, "Tarkowski", 3, 3 },
                    { 23, "Luka Milivojevic", 3, 2 },
                    { 13, "Luka Milivojevic", 3, 1 },
                    { 62, "Oktober", 2, 6 },
                    { 52, "November", 2, 5 },
                    { 32, "November", 2, 3 },
                    { 22, "November", 2, 2 },
                    { 12, "Desember", 2, 1 },
                    { 61, "Dean Smith", 1, 6 },
                    { 51, "David Moyes", 1, 5 },
                    { 41, "Sean Dyche", 1, 4 },
                    { 31, "Roy Hodgson", 1, 3 },
                    { 21, "David Moyes", 1, 2 },
                    { 11, "Roy Hodgson", 1, 1 },
                    { 517, "Kevin De Bruyne", 17, 5 },
                    { 24, "Leeds", 4, 2 },
                    { 34, "Burnley", 4, 3 },
                    { 43, "David Luiz", 3, 4 },
                    { 54, "Tottenham Hotspur", 4, 5 },
                    { 18, "Over", 8, 1 },
                    { 67, "Reece James", 7, 6 },
                    { 57, "Lindeløf", 7, 5 },
                    { 47, "Jesse Lingaard", 7, 4 },
                    { 37, "Soyuncu", 7, 3 },
                    { 44, "Arsenal", 4, 4 },
                    { 17, "Harry Maguire", 7, 1 },
                    { 66, "Kevin De Bruyne", 6, 6 },
                    { 56, "Kevin De Bruyne", 6, 5 },
                    { 46, "Kevin De Bruyne", 6, 4 },
                    { 27, "Aaron Wan-Bissaka", 7, 2 },
                    { 36, "Kevin De Bruyne", 6, 3 },
                    { 26, "Kevin De Bruyne", 6, 2 },
                    { 16, "Kevin De Bruyne", 6, 1 },
                    { 65, "Brighton", 5, 6 },
                    { 55, "Arsenal", 5, 5 },
                    { 45, "Arsenal", 5, 4 },
                    { 35, "Fulham", 5, 3 },
                    { 25, "Arsenal", 5, 2 },
                    { 15, "Tottenham", 5, 1 },
                    { 64, "Burnley", 4, 6 }
                });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "Id", "SeasonGuessId", "p1", "p10", "p11", "p12", "p13", "p14", "p15", "p16", "p17", "p18", "p19", "p2", "p20", "p3", "p4", "p5", "p6", "p7", "p8", "p9" },
                values: new object[,]
                {
                    { 5, 5, "11", "9", "4", "16", "15", "6", "14", "19", "2", "3", "18", "12", "8", "13", "5", "17", "1", "20", "10", "7" },
                    { 1, 1, "12", "16", "15", "4", "3", "19", "14", "2", "6", "9", "8", "11", "18", "13", "5", "1", "17", "10", "7", "20" },
                    { 3, 3, "12", "4", "9", "16", "6", "14", "19", "2", "15", "3", "8", "11", "18", "13", "5", "17", "1", "10", "20", "7" },
                    { 6, 6, "12", "9", "16", "15", "6", "4", "14", "8", "19", "2", "3", "5", "8", "11", "13", "17", "1", "7", "10", "20" },
                    { 4, 4, "11", "7", "6", "15", "19", "16", "9", "8", "2", "18", "3", "12", "4", "5", "13", "1", "10", "17", "20", "14" },
                    { 2, 2, "12", "10", "16", "4", "3", "15", "6", "19", "18", "14", "2", "11", "8", "5", "13", "17", "1", "20", "7", "9" }
                });

            migrationBuilder.InsertData(
                table: "TopScorers",
                columns: new[] { "Id", "Goals1", "Goals2", "Goals3", "Goals4", "Goals5", "Scorer1", "Scorer2", "Scorer3", "Scorer4", "Scorer5", "SeasonGuessId" },
                values: new object[,]
                {
                    { 3, 24, 22, 20, 18, 17, "Aubameyang", "Sterling", "Salah", "Mane", "Aguero", 3 },
                    { 1, 25, 22, 21, 21, 20, "Aubameyang", "Salah", "Werner", "Sterling", "Kane", 1 },
                    { 5, 28, 26, 26, 24, 22, "Salah", "Aubameyang", "Kane", "Sterling", "Mane", 5 },
                    { 6, 26, 23, 21, 21, 19, "Kane", "Salah", "Werner", "Aubemeyang", "Rashford", 6 },
                    { 4, 23, 21, 20, 20, 18, "Mane", "Werner", "Salah", "Aubameyang", "Sterling", 4 },
                    { 2, 25, 23, 22, 21, 19, "Aubameyang", "Sterling", "Kane", "Salah", "Werner", 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameWeeks_SeasonId",
                table: "GameWeeks",
                column: "SeasonId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalScorers_GameWeekId",
                table: "GoalScorers",
                column: "GameWeekId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalScorers_SeasonGuessId",
                table: "GoalScorers",
                column: "SeasonGuessId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchGuesses_GameWeekId",
                table: "MatchGuesses",
                column: "GameWeekId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchGuesses_SeasonGuessId",
                table: "MatchGuesses",
                column: "SeasonGuessId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionAnswers_QuestionId",
                table: "QuestionAnswers",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionAnswers_SeasonGuessId",
                table: "QuestionAnswers",
                column: "SeasonGuessId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_SeasonId",
                table: "Questions",
                column: "SeasonId");

            migrationBuilder.CreateIndex(
                name: "IX_SeasonGuesses_GuesserId",
                table: "SeasonGuesses",
                column: "GuesserId");

            migrationBuilder.CreateIndex(
                name: "IX_SeasonGuesses_SeasonId",
                table: "SeasonGuesses",
                column: "SeasonId");

            migrationBuilder.CreateIndex(
                name: "IX_Tables_SeasonGuessId",
                table: "Tables",
                column: "SeasonGuessId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TopScorers_SeasonGuessId",
                table: "TopScorers",
                column: "SeasonGuessId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GoalScorers");

            migrationBuilder.DropTable(
                name: "MatchGuesses");

            migrationBuilder.DropTable(
                name: "QuestionAnswers");

            migrationBuilder.DropTable(
                name: "Tables");

            migrationBuilder.DropTable(
                name: "TopScorers");

            migrationBuilder.DropTable(
                name: "GameWeeks");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "SeasonGuesses");

            migrationBuilder.DropTable(
                name: "Guessers");

            migrationBuilder.DropTable(
                name: "Seasons");
        }
    }
}
