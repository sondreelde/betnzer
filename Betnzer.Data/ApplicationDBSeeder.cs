using System;
using Betnzer.Data.Models;
using Betnzer.Data.Models.Guess;
using Microsoft.EntityFrameworkCore;

namespace Betnzer.Data
{
    public class ApplicationDBSeeder
    {
        public static void Seed(ModelBuilder modelBuilder){
            modelBuilder.Entity<Season>().HasData(
                new Season(){Id = 1, Name = "20/21"}
            );

            modelBuilder.Entity<Question>().HasData(
                new Question(){Id = 1, SeasonId = 1, Short = "Sacked", Text = "Første manager til å forlate sin klubb?"},
                new Question(){Id = 2, SeasonId = 1, Short = "Sacked, when?", Text = "I hvilken måned forlater den første manageren sin klubb?"},
                new Question(){Id = 3, SeasonId = 1, Short = "Gult kort!", Text = "Hvilken spiller får flest gule kort?"},
                new Question(){Id = 4, SeasonId = 1, Short = "Play like cunts", Text = "Hvilket lag får flest gule kort?"},
                new Question(){Id = 5, SeasonId = 1, Short = "Røde kort(lag)", Text = "Hvilket lag får flest røde kort?"},
                new Question(){Id = 6, SeasonId = 1, Short = "Assist", Text = "Hvilken spiller får flest assist? Vunnet straffe telles ikke. (OPTA)"},
                new Question(){Id = 7, SeasonId = 1, Short = "One hit wonder", Text = "Nevn én spiller som scorer nøyaktig ett mål."},
                new Question(){Id = 8, SeasonId = 1, Short = "Boxing goals", Text = "[Over/Under] 19.5 blir scoret på boxing day?"},
                new Question(){Id = 9, SeasonId = 1, Short = "Leeds solid?", Text = "Leeds slipper inn 5 eller flere mål i en kamp?"},
                new Question(){Id = 10, SeasonId = 1, Short = "Leicester galore?", Text = "Leicester scorer 5 eller flere mål i en kamp?"},
                new Question(){Id = 11, SeasonId = 1, Short = "Clean sheets", Text = "Hvilken keeper får flest clean sheets?"},
                new Question(){Id = 12, SeasonId = 1, Short = "Avgjort", Text = "I hvilken runde blir Premier League trofeét sikret? (FPL-runde)"},
                new Question(){Id = 13, SeasonId = 1, Short = "Flest mål", Text = "Hvem scorer flest mål av [ Barnes / Trezeguet / McBurnie ]?"},
                new Question(){Id = 14, SeasonId = 1, Short = "Flest mål v.2", Text = "Hvem scorer flest mål av [ Iheanacho / Wesley / Yarmolenko ]?"},
                new Question(){Id = 15, SeasonId = 1, Short = "FA-cupen", Text = "Hvilket lag vinner FA-cup finalen?"},
                new Question(){Id = 16, SeasonId = 1, Short = "Ligacupen", Text = "Hvilket lag vinner ligacupen?"},
                new Question(){Id = 17, SeasonId = 1, Short = "PFA", Text = "Hvilken spiller blir kåret til PFA player of the year?"}
            );

            modelBuilder.Entity<GameWeek>().HasData(
                new GameWeek() {Id = 1, WeekNumber = 1, SeasonId = 1, HomeTeam = "6", AwayTeam = "16", Result='h'},
                new GameWeek() {Id = 2, WeekNumber = 2, SeasonId = 1, HomeTeam = "5", AwayTeam = "11", Result='b'},
                new GameWeek() {Id = 3, WeekNumber = 3, SeasonId = 1, HomeTeam = "19", AwayTeam = "20", Result='h'},
                new GameWeek() {Id = 4, WeekNumber = 4, SeasonId = 1, HomeTeam = "13", AwayTeam = "17", Result='b'},
                new GameWeek() {Id = 5, WeekNumber = 5, SeasonId = 1, HomeTeam = "6", AwayTeam = "3", Result='x'},
                new GameWeek() {Id = 6, WeekNumber = 6, SeasonId = 1, HomeTeam = "13", AwayTeam = "5", Result='x'},
                new GameWeek() {Id = 7, WeekNumber = 7, SeasonId = 1, HomeTeam = "13", AwayTeam = "1", Result='x'},
                new GameWeek() {Id = 8, WeekNumber = 8, SeasonId = 1, HomeTeam = "12", AwayTeam = "11", Result='x'},
                new GameWeek() {Id = 9, WeekNumber = 9, SeasonId = 1, HomeTeam = "2", AwayTeam = "3", Result='x'},
                new GameWeek() {Id = 10, WeekNumber = 10, SeasonId = 1, HomeTeam = "5", AwayTeam = "17", Result='x'},
                new GameWeek() {Id = 11, WeekNumber = 11, SeasonId = 1, HomeTeam = "17", AwayTeam = "1", Result='x'},
                new GameWeek() {Id = 12, WeekNumber = 12, SeasonId = 1, HomeTeam = "13", AwayTeam = "12", Result='x'},
                new GameWeek() {Id = 13, WeekNumber = 13, SeasonId = 1, HomeTeam = "9", AwayTeam = "14", Result='x'},
                new GameWeek() {Id = 14, WeekNumber = 14, SeasonId = 1, HomeTeam = "4", AwayTeam = "20", Result='x'},
                new GameWeek() {Id = 15, WeekNumber = 15, SeasonId = 1, HomeTeam = "1", AwayTeam = "5", Result='x'},
                new GameWeek() {Id = 16, WeekNumber = 16, SeasonId = 1, HomeTeam = "18", AwayTeam = "9", Result='x'},
                new GameWeek() {Id = 17, WeekNumber = 17, SeasonId = 1, HomeTeam = "5", AwayTeam = "12", Result='x'},
                new GameWeek() {Id = 18, WeekNumber = 18, SeasonId = 1, HomeTeam = "20", AwayTeam = "7", Result='x'},
                new GameWeek() {Id = 19, WeekNumber = 19, SeasonId = 1, HomeTeam = "15", AwayTeam = "17", Result='x'},
                new GameWeek() {Id = 20, WeekNumber = 20, SeasonId = 1, HomeTeam = "7", AwayTeam = "10", Result='x'},
                new GameWeek() {Id = 21, WeekNumber = 21, SeasonId = 1, HomeTeam = "1", AwayTeam = "13", Result='x'},
                new GameWeek() {Id = 22, WeekNumber = 22, SeasonId = 1, HomeTeam = "17", AwayTeam = "5", Result='x'},
                new GameWeek() {Id = 23, WeekNumber = 23, SeasonId = 1, HomeTeam = "11", AwayTeam = "12", Result='x'},
                new GameWeek() {Id = 24, WeekNumber = 24, SeasonId = 1, HomeTeam = "3", AwayTeam = "2", Result='x'},
                new GameWeek() {Id = 25, WeekNumber = 25, SeasonId = 1, HomeTeam = "3", AwayTeam = "6", Result='x'},
                new GameWeek() {Id = 26, WeekNumber = 26, SeasonId = 1, HomeTeam = "10", AwayTeam = "1", Result='x'},
                new GameWeek() {Id = 27, WeekNumber = 27, SeasonId = 1, HomeTeam = "19", AwayTeam = "9", Result='x'},
                new GameWeek() {Id = 28, WeekNumber = 28, SeasonId = 1, HomeTeam = "1", AwayTeam = "17", Result='x'},
                new GameWeek() {Id = 29, WeekNumber = 29, SeasonId = 1, HomeTeam = "19", AwayTeam = "1", Result='x'},
                new GameWeek() {Id = 30, WeekNumber = 30, SeasonId = 1, HomeTeam = "9", AwayTeam = "15", Result='x'},
                new GameWeek() {Id = 31, WeekNumber = 31, SeasonId = 1, HomeTeam = "4", AwayTeam = "13", Result='x'},
                new GameWeek() {Id = 32, WeekNumber = 32, SeasonId = 1, HomeTeam = "7", AwayTeam = "17", Result='x'},
                new GameWeek() {Id = 33, WeekNumber = 33, SeasonId = 1, HomeTeam = "20", AwayTeam = "4", Result='x'},
                new GameWeek() {Id = 34, WeekNumber = 34, SeasonId = 1, HomeTeam = "3", AwayTeam = "10", Result='x'},
                new GameWeek() {Id = 35, WeekNumber = 35, SeasonId = 1, HomeTeam = "19", AwayTeam = "7", Result='x'},
                new GameWeek() {Id = 36, WeekNumber = 36, SeasonId = 1, HomeTeam = "7", AwayTeam = "15", Result='x'},
                new GameWeek() {Id = 37, WeekNumber = 37, SeasonId = 1, HomeTeam = "18", AwayTeam = "9", Result='x'},
                new GameWeek() {Id = 38, WeekNumber = 38, SeasonId = 1, HomeTeam = "10", AwayTeam = "17", Result='x'}
            );


            AddSondre(modelBuilder);
            AddØystein(modelBuilder);
            AddIvar(modelBuilder);
            AddMakke(modelBuilder);
            AddMarty(modelBuilder);
            AddBenny(modelBuilder);

            
        }

        private static void AddSondre(ModelBuilder modelBuilder)
        {
            int guesserNumber = 1;


            modelBuilder.Entity<Guesser>().HasData(
                new Guesser(){ Id = guesserNumber, Name = "Sondre"}
            );

            modelBuilder.Entity<SeasonGuess>().HasData(
                new SeasonGuess(){ Id = guesserNumber, SeasonId = 1, GuesserId = guesserNumber}
            );

            modelBuilder.Entity<TopScorers>().HasData(
                new TopScorers() {
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    Scorer1 = "Aubameyang", Goals1 = 25,
                    Scorer2 = "Salah", Goals2 = 22,
                    Scorer3 = "Werner", Goals3 =  21,
                    Scorer4 = "Sterling", Goals4 = 21,
                    Scorer5 = "Kane", Goals5 = 20
                }
            );

            modelBuilder.Entity<Table>().HasData(
                new Table(){
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    p1 = "12", p2 = "11", p3 = "13", p4 = "5", p5 = "1",
                    p6 = "17", p7 = "10", p8 = "7", p9 = "20", p10 = "16",
                    p11 = "15", p12 = "4", p13 = "3", p14 = "19", p15 = "14",
                    p16 = "2", p17 = "6", p18 = "9", p19 = "8", p20 = "18"
                }
            );

            modelBuilder.Entity<QuestionAnswer>().HasData(
                new QuestionAnswer(){ Id = 11, QuestionId = 1, SeasonGuessId = guesserNumber, Ans = "Roy Hodgson"},
                new QuestionAnswer(){ Id = 12, QuestionId = 2, SeasonGuessId = guesserNumber, Ans = "Desember"},
                new QuestionAnswer(){ Id = 13, QuestionId = 3, SeasonGuessId = guesserNumber, Ans = "Luka Milivojevic"},
                new QuestionAnswer(){ Id = 14, QuestionId = 4, SeasonGuessId = guesserNumber, Ans = "Arsenal"},
                new QuestionAnswer(){ Id = 15, QuestionId = 5, SeasonGuessId = guesserNumber, Ans = "Tottenham"},
                new QuestionAnswer(){ Id = 16, QuestionId = 6, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"},
                new QuestionAnswer(){ Id = 17, QuestionId = 7, SeasonGuessId = guesserNumber, Ans = "Harry Maguire"},
                new QuestionAnswer(){ Id = 18, QuestionId = 8, SeasonGuessId = guesserNumber, Ans = "Over"},
                new QuestionAnswer(){ Id = 19, QuestionId = 9, SeasonGuessId = guesserNumber, Ans = "Ja"},
                new QuestionAnswer(){ Id = 110, QuestionId = 10, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 111, QuestionId = 11, SeasonGuessId = guesserNumber, Ans = "Ederson"},
                new QuestionAnswer(){ Id = 112, QuestionId = 12, SeasonGuessId = guesserNumber, Ans = "37"},
                new QuestionAnswer(){ Id = 113, QuestionId = 13, SeasonGuessId = guesserNumber, Ans = "Trezeguet"},
                new QuestionAnswer(){ Id = 114, QuestionId = 14, SeasonGuessId = guesserNumber, Ans = "Wesley"},
                new QuestionAnswer(){ Id = 115, QuestionId = 15, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 116, QuestionId = 16, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 117, QuestionId = 17, SeasonGuessId = guesserNumber, Ans = "Bruno Fernandes"}
            );

            modelBuilder.Entity<MatchGuess>().HasData(
                new MatchGuess() { Id = 11, SeasonGuessId = guesserNumber, GameWeekId = 1, Res = 'b'},
                new MatchGuess() { Id = 12, SeasonGuessId = guesserNumber, GameWeekId = 2, Res = 'b'},
                new MatchGuess() { Id = 13, SeasonGuessId = guesserNumber, GameWeekId = 3, Res = 'b'},
                new MatchGuess() { Id = 14, SeasonGuessId = guesserNumber, GameWeekId = 4, Res = 'h'},
                new MatchGuess() { Id = 15, SeasonGuessId = guesserNumber, GameWeekId = 5, Res = 'u'},
                new MatchGuess() { Id = 16, SeasonGuessId = guesserNumber, GameWeekId = 6, Res = 'h'},
                new MatchGuess() { Id = 17, SeasonGuessId = guesserNumber, GameWeekId = 7, Res = 'h'},
                new MatchGuess() { Id = 18, SeasonGuessId = guesserNumber, GameWeekId = 8, Res = 'h'},
                new MatchGuess() { Id = 19, SeasonGuessId = guesserNumber, GameWeekId = 9, Res = 'u'},
                new MatchGuess() { Id = 110, SeasonGuessId = guesserNumber, GameWeekId = 10, Res = 'h'},
                new MatchGuess() { Id = 111, SeasonGuessId = guesserNumber, GameWeekId = 11, Res = 'b'},
                new MatchGuess() { Id = 112, SeasonGuessId = guesserNumber, GameWeekId = 12, Res = 'u'},
                new MatchGuess() { Id = 113, SeasonGuessId = guesserNumber, GameWeekId = 13, Res = 'b'},
                new MatchGuess() { Id = 114, SeasonGuessId = guesserNumber, GameWeekId = 14, Res = 'b'},
                new MatchGuess() { Id = 115, SeasonGuessId = guesserNumber, GameWeekId = 15, Res = 'b'},
                new MatchGuess() { Id = 116, SeasonGuessId = guesserNumber, GameWeekId = 16, Res = 'b'},
                new MatchGuess() { Id = 117, SeasonGuessId = guesserNumber, GameWeekId = 17, Res = 'b'},
                new MatchGuess() { Id = 118, SeasonGuessId = guesserNumber, GameWeekId = 18, Res = 'b'},
                new MatchGuess() { Id = 119, SeasonGuessId = guesserNumber, GameWeekId = 19, Res = 'b'},
                new MatchGuess() { Id = 120, SeasonGuessId = guesserNumber, GameWeekId = 20, Res = 'u'},
                new MatchGuess() { Id = 121, SeasonGuessId = guesserNumber, GameWeekId = 21, Res = 'b'},
                new MatchGuess() { Id = 122, SeasonGuessId = guesserNumber, GameWeekId = 22, Res = 'b'},
                new MatchGuess() { Id = 123, SeasonGuessId = guesserNumber, GameWeekId = 23, Res = 'b'},
                new MatchGuess() { Id = 124, SeasonGuessId = guesserNumber, GameWeekId = 24, Res = 'u'},
                new MatchGuess() { Id = 125, SeasonGuessId = guesserNumber, GameWeekId = 25, Res = 'u'},
                new MatchGuess() { Id = 126, SeasonGuessId = guesserNumber, GameWeekId = 26, Res = 'b'},
                new MatchGuess() { Id = 127, SeasonGuessId = guesserNumber, GameWeekId = 27, Res = 'h'},
                new MatchGuess() { Id = 128, SeasonGuessId = guesserNumber, GameWeekId = 28, Res = 'h'},
                new MatchGuess() { Id = 129, SeasonGuessId = guesserNumber, GameWeekId = 29, Res = 'b'},
                new MatchGuess() { Id = 130, SeasonGuessId = guesserNumber, GameWeekId = 30, Res = 'b'},
                new MatchGuess() { Id = 131, SeasonGuessId = guesserNumber, GameWeekId = 31, Res = 'b'},
                new MatchGuess() { Id = 132, SeasonGuessId = guesserNumber, GameWeekId = 32, Res = 'u'},
                new MatchGuess() { Id = 133, SeasonGuessId = guesserNumber, GameWeekId = 33, Res = 'h'},
                new MatchGuess() { Id = 134, SeasonGuessId = guesserNumber, GameWeekId = 34, Res = 'b'},
                new MatchGuess() { Id = 135, SeasonGuessId = guesserNumber, GameWeekId = 35, Res = 'b'},
                new MatchGuess() { Id = 136, SeasonGuessId = guesserNumber, GameWeekId = 36, Res = 'h'},
                new MatchGuess() { Id = 137, SeasonGuessId = guesserNumber, GameWeekId = 37, Res = 'b'},
                new MatchGuess() { Id = 138, SeasonGuessId = guesserNumber, GameWeekId = 38, Res = 'b'}
            );

            modelBuilder.Entity<GoalScorers>().HasData(
                new GoalScorers() { Id = 11, SeasonGuessId = guesserNumber, GameWeekId = 1, Name = "Roberto Firmino"},
                new GoalScorers() { Id = 12, SeasonGuessId = guesserNumber, GameWeekId = 2, Name = "Richarlison"},
                new GoalScorers() { Id = 13, SeasonGuessId = guesserNumber, GameWeekId = 3, Name = "Mason Mount"},
                new GoalScorers() { Id = 14, SeasonGuessId = guesserNumber, GameWeekId = 4, Name = "Raúl Jiménez"},
                new GoalScorers() { Id = 15, SeasonGuessId = guesserNumber, GameWeekId = 5, Name = "Chris Wood"},
                new GoalScorers() { Id = 16, SeasonGuessId = guesserNumber, GameWeekId = 6, Name = "Neal Maupay"},
                new GoalScorers() { Id = 17, SeasonGuessId = guesserNumber, GameWeekId = 7, Name = "Ayoze Pérez"},
                new GoalScorers() { Id = 18, SeasonGuessId = guesserNumber, GameWeekId = 8, Name = "Dele Alli"},
                new GoalScorers() { Id = 19, SeasonGuessId = guesserNumber, GameWeekId = 9, Name = "Nicolas Pépé"},
                new GoalScorers() { Id = 110, SeasonGuessId = guesserNumber, GameWeekId = 10, Name = "Jamie Vardy"},
                new GoalScorers() { Id = 111, SeasonGuessId = guesserNumber, GameWeekId = 11, Name = "Christian Pulisic"},
                new GoalScorers() { Id = 112, SeasonGuessId = guesserNumber, GameWeekId = 12, Name = "Michail Antonio"},
                new GoalScorers() { Id = 113, SeasonGuessId = guesserNumber, GameWeekId = 13, Name = "Phil Foden"},
                new GoalScorers() { Id = 114, SeasonGuessId = guesserNumber, GameWeekId = 14, Name = "Mason Greenwood"},
                new GoalScorers() { Id = 115, SeasonGuessId = guesserNumber, GameWeekId = 15, Name = "Danny Ings"},
                new GoalScorers() { Id = 116, SeasonGuessId = guesserNumber, GameWeekId = 16, Name = "Steven Bergwijn"},
                new GoalScorers() { Id = 117, SeasonGuessId = guesserNumber, GameWeekId = 17, Name = "Alexandre Lacazette"},
                new GoalScorers() { Id = 118, SeasonGuessId = guesserNumber, GameWeekId = 18, Name = "Sébastien Haller"},
                new GoalScorers() { Id = 119, SeasonGuessId = guesserNumber, GameWeekId = 19, Name = "Diogo Jota"},
                new GoalScorers() { Id = 120, SeasonGuessId = guesserNumber, GameWeekId = 20, Name = "Sergio Agüero"},
                new GoalScorers() { Id = 121, SeasonGuessId = guesserNumber, GameWeekId = 21, Name = "Sadio Mané"},
                new GoalScorers() { Id = 122, SeasonGuessId = guesserNumber, GameWeekId = 22, Name = "Dominic Calvert-Lewin"},
                new GoalScorers() { Id = 123, SeasonGuessId = guesserNumber, GameWeekId = 23, Name = "Harry Kane"},
                new GoalScorers() { Id = 124, SeasonGuessId = guesserNumber, GameWeekId = 24, Name = "Marcus Rashford"},
                new GoalScorers() { Id = 125, SeasonGuessId = guesserNumber, GameWeekId = 25, Name = "Son Heung-Min"},
                new GoalScorers() { Id = 126, SeasonGuessId = guesserNumber, GameWeekId = 26, Name = "Leandro Trossard"},
                new GoalScorers() { Id = 127, SeasonGuessId = guesserNumber, GameWeekId = 27, Name = "Mohamed Salah"},
                new GoalScorers() { Id = 128, SeasonGuessId = guesserNumber, GameWeekId = 28, Name = "Jordan Ayew"},
                new GoalScorers() { Id = 129, SeasonGuessId = guesserNumber, GameWeekId = 29, Name = "Aleksandar Mitrović"},
                new GoalScorers() { Id = 130, SeasonGuessId = guesserNumber, GameWeekId = 30, Name = "Timo Werner"},
                new GoalScorers() { Id = 131, SeasonGuessId = guesserNumber, GameWeekId = 31, Name = "Che Adams"},
                new GoalScorers() { Id = 132, SeasonGuessId = guesserNumber, GameWeekId = 32, Name = "Raheem Sterling"},
                new GoalScorers() { Id = 133, SeasonGuessId = guesserNumber, GameWeekId = 33, Name = "Jack Grealish"},
                new GoalScorers() { Id = 134, SeasonGuessId = guesserNumber, GameWeekId = 34, Name = "Hakim Ziyech"},
                new GoalScorers() { Id = 135, SeasonGuessId = guesserNumber, GameWeekId = 35, Name = "Jay Rodriguez"},
                new GoalScorers() { Id = 136, SeasonGuessId = guesserNumber, GameWeekId = 36, Name = "Kevin De Bruyne"},
                new GoalScorers() { Id = 137, SeasonGuessId = guesserNumber, GameWeekId = 37, Name = "Anthony Martial"},
                new GoalScorers() { Id = 138, SeasonGuessId = guesserNumber, GameWeekId = 38, Name = "Rodrigo Moreno Machado"}
            );
        }

        private static void AddØystein(ModelBuilder modelBuilder)
        {
            int guesserNumber = 2;


            modelBuilder.Entity<Guesser>().HasData(
                new Guesser(){ Id = guesserNumber, Name = "Øystein"}
            );

            modelBuilder.Entity<SeasonGuess>().HasData(
                new SeasonGuess(){ Id = guesserNumber, SeasonId = 1, GuesserId = guesserNumber}
            );

            modelBuilder.Entity<TopScorers>().HasData(
                new TopScorers() {
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    Scorer1 = "Aubameyang", Goals1 = 25,
                    Scorer2 = "Sterling", Goals2 = 23,
                    Scorer3 = "Kane", Goals3 =  22,
                    Scorer4 = "Salah", Goals4 = 21,
                    Scorer5 = "Werner", Goals5 = 19
                }
            );

            modelBuilder.Entity<Table>().HasData(
                new Table(){
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    p1 = "12", p2 = "11", p3 = "5", p4 = "13", p5 = "17",
                    p6 = "1", p7 = "20", p8 = "7", p9 = "9", p10 = "10",
                    p11 = "16", p12 = "4", p13 = "3", p14 = "15", p15 = "6",
                    p16 = "19", p17 = "18", p18 = "14", p19 = "2", p20 = "8"
                }
            );

            modelBuilder.Entity<QuestionAnswer>().HasData(
                new QuestionAnswer(){ Id = 21, QuestionId = 1, SeasonGuessId = guesserNumber, Ans = "David Moyes"},
                new QuestionAnswer(){ Id = 22, QuestionId = 2, SeasonGuessId = guesserNumber, Ans = "November"},
                new QuestionAnswer(){ Id = 23, QuestionId = 3, SeasonGuessId = guesserNumber, Ans = "Luka Milivojevic"},
                new QuestionAnswer(){ Id = 24, QuestionId = 4, SeasonGuessId = guesserNumber, Ans = "Leeds"},
                new QuestionAnswer(){ Id = 25, QuestionId = 5, SeasonGuessId = guesserNumber, Ans = "Arsenal"},
                new QuestionAnswer(){ Id = 26, QuestionId = 6, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"},
                new QuestionAnswer(){ Id = 27, QuestionId = 7, SeasonGuessId = guesserNumber, Ans = "Aaron Wan-Bissaka"},
                new QuestionAnswer(){ Id = 28, QuestionId = 8, SeasonGuessId = guesserNumber, Ans = "Over"},
                new QuestionAnswer(){ Id = 29, QuestionId = 9, SeasonGuessId = guesserNumber, Ans = "Ja"},
                new QuestionAnswer(){ Id = 210, QuestionId = 10, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 211, QuestionId = 11, SeasonGuessId = guesserNumber, Ans = "Ederson"},
                new QuestionAnswer(){ Id = 212, QuestionId = 12, SeasonGuessId = guesserNumber, Ans = "35"},
                new QuestionAnswer(){ Id = 213, QuestionId = 13, SeasonGuessId = guesserNumber, Ans = "McBurnie"},
                new QuestionAnswer(){ Id = 214, QuestionId = 14, SeasonGuessId = guesserNumber, Ans = "Yarmolenko"},
                new QuestionAnswer(){ Id = 215, QuestionId = 15, SeasonGuessId = guesserNumber, Ans = "Chelsea"},
                new QuestionAnswer(){ Id = 216, QuestionId = 16, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 217, QuestionId = 17, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"}
            );

            modelBuilder.Entity<MatchGuess>().HasData(
                new MatchGuess() { Id = 21, SeasonGuessId = guesserNumber, GameWeekId = 1, Res = 'b'},
                new MatchGuess() { Id = 22, SeasonGuessId = guesserNumber, GameWeekId = 2, Res = 'b'},
                new MatchGuess() { Id = 23, SeasonGuessId = guesserNumber, GameWeekId = 3, Res = 'b'},
                new MatchGuess() { Id = 24, SeasonGuessId = guesserNumber, GameWeekId = 4, Res = 'h'},
                new MatchGuess() { Id = 25, SeasonGuessId = guesserNumber, GameWeekId = 5, Res = 'h'},
                new MatchGuess() { Id = 26, SeasonGuessId = guesserNumber, GameWeekId = 6, Res = 'u'},
                new MatchGuess() { Id = 27, SeasonGuessId = guesserNumber, GameWeekId = 7, Res = 'h'},
                new MatchGuess() { Id = 28, SeasonGuessId = guesserNumber, GameWeekId = 8, Res = 'u'},
                new MatchGuess() { Id = 29, SeasonGuessId = guesserNumber, GameWeekId = 9, Res = 'b'},
                new MatchGuess() { Id = 210, SeasonGuessId = guesserNumber, GameWeekId = 10, Res = 'h'},
                new MatchGuess() { Id = 211, SeasonGuessId = guesserNumber, GameWeekId = 11, Res = 'u'},
                new MatchGuess() { Id = 212, SeasonGuessId = guesserNumber, GameWeekId = 12, Res = 'b'},
                new MatchGuess() { Id = 213, SeasonGuessId = guesserNumber, GameWeekId = 13, Res = 'h'},
                new MatchGuess() { Id = 214, SeasonGuessId = guesserNumber, GameWeekId = 14, Res = 'b'},
                new MatchGuess() { Id = 215, SeasonGuessId = guesserNumber, GameWeekId = 15, Res = 'h'},
                new MatchGuess() { Id = 216, SeasonGuessId = guesserNumber, GameWeekId = 16, Res = 'b'},
                new MatchGuess() { Id = 217, SeasonGuessId = guesserNumber, GameWeekId = 17, Res = 'h'},
                new MatchGuess() { Id = 218, SeasonGuessId = guesserNumber, GameWeekId = 18, Res = 'h'},
                new MatchGuess() { Id = 219, SeasonGuessId = guesserNumber, GameWeekId = 19, Res = 'b'},
                new MatchGuess() { Id = 220, SeasonGuessId = guesserNumber, GameWeekId = 20, Res = 'h'},
                new MatchGuess() { Id = 221, SeasonGuessId = guesserNumber, GameWeekId = 21, Res = 'h'},
                new MatchGuess() { Id = 222, SeasonGuessId = guesserNumber, GameWeekId = 22, Res = 'b'},
                new MatchGuess() { Id = 223, SeasonGuessId = guesserNumber, GameWeekId = 23, Res = 'u'},
                new MatchGuess() { Id = 224, SeasonGuessId = guesserNumber, GameWeekId = 24, Res = 'h'},
                new MatchGuess() { Id = 225, SeasonGuessId = guesserNumber, GameWeekId = 25, Res = 'h'},
                new MatchGuess() { Id = 226, SeasonGuessId = guesserNumber, GameWeekId = 26, Res = 'b'},
                new MatchGuess() { Id = 227, SeasonGuessId = guesserNumber, GameWeekId = 27, Res = 'b'},
                new MatchGuess() { Id = 228, SeasonGuessId = guesserNumber, GameWeekId = 28, Res = 'h'},
                new MatchGuess() { Id = 229, SeasonGuessId = guesserNumber, GameWeekId = 29, Res = 'b'},
                new MatchGuess() { Id = 230, SeasonGuessId = guesserNumber, GameWeekId = 30, Res = 'h'},
                new MatchGuess() { Id = 231, SeasonGuessId = guesserNumber, GameWeekId = 31, Res = 'b'},
                new MatchGuess() { Id = 232, SeasonGuessId = guesserNumber, GameWeekId = 32, Res = 'b'},
                new MatchGuess() { Id = 233, SeasonGuessId = guesserNumber, GameWeekId = 33, Res = 'h'},
                new MatchGuess() { Id = 234, SeasonGuessId = guesserNumber, GameWeekId = 34, Res = 'b'},
                new MatchGuess() { Id = 235, SeasonGuessId = guesserNumber, GameWeekId = 35, Res = 'b'},
                new MatchGuess() { Id = 236, SeasonGuessId = guesserNumber, GameWeekId = 36, Res = 'h'},
                new MatchGuess() { Id = 237, SeasonGuessId = guesserNumber, GameWeekId = 37, Res = 'h'},
                new MatchGuess() { Id = 238, SeasonGuessId = guesserNumber, GameWeekId = 38, Res = 'b'}
            );

            modelBuilder.Entity<GoalScorers>().HasData(
                new GoalScorers() { Id = 21, SeasonGuessId = guesserNumber, GameWeekId = 1, Name = "Sadio Mane"},
                new GoalScorers() { Id = 22, SeasonGuessId = guesserNumber, GameWeekId = 2, Name = "Anthony Martial"},
                new GoalScorers() { Id = 23, SeasonGuessId = guesserNumber, GameWeekId = 3, Name = "Heung-Min Son"},
                new GoalScorers() { Id = 24, SeasonGuessId = guesserNumber, GameWeekId = 4, Name = "Timo Werner"},
                new GoalScorers() { Id = 25, SeasonGuessId = guesserNumber, GameWeekId = 5, Name = "Harry Kane"},
                new GoalScorers() { Id = 26, SeasonGuessId = guesserNumber, GameWeekId = 6, Name = "Roberto Firmino"},
                new GoalScorers() { Id = 27, SeasonGuessId = guesserNumber, GameWeekId = 7, Name = "Raul Jimenez"},
                new GoalScorers() { Id = 28, SeasonGuessId = guesserNumber, GameWeekId = 8, Name = "Sebastien Haller"},
                new GoalScorers() { Id = 29, SeasonGuessId = guesserNumber, GameWeekId = 9, Name = "Adama Traore"},
                new GoalScorers() { Id = 210, SeasonGuessId = guesserNumber, GameWeekId = 10, Name = "Sergio Aguero"},
                new GoalScorers() { Id = 211, SeasonGuessId = guesserNumber, GameWeekId = 11, Name = "Raheem Sterling"},
                new GoalScorers() { Id = 212, SeasonGuessId = guesserNumber, GameWeekId = 12, Name = "Willian"},
                new GoalScorers() { Id = 213, SeasonGuessId = guesserNumber, GameWeekId = 13, Name = "Gabriel Jesus"},
                new GoalScorers() { Id = 214, SeasonGuessId = guesserNumber, GameWeekId = 14, Name = "Hakim Ziyech"},
                new GoalScorers() { Id = 215, SeasonGuessId = guesserNumber, GameWeekId = 15, Name = "Kevin de Bruyne"},
                new GoalScorers() { Id = 216, SeasonGuessId = guesserNumber, GameWeekId = 16, Name = "Danny Ings"},
                new GoalScorers() { Id = 217, SeasonGuessId = guesserNumber, GameWeekId = 17, Name = "Chris Wood"},
                new GoalScorers() { Id = 218, SeasonGuessId = guesserNumber, GameWeekId = 18, Name = "Mohamed Salah"},
                new GoalScorers() { Id = 219, SeasonGuessId = guesserNumber, GameWeekId = 19, Name = "Jamie Vardy"},
                new GoalScorers() { Id = 220, SeasonGuessId = guesserNumber, GameWeekId = 20, Name = "Mason Greenwood"},
                new GoalScorers() { Id = 221, SeasonGuessId = guesserNumber, GameWeekId = 21, Name = "James Rodriguez"},
                new GoalScorers() { Id = 222, SeasonGuessId = guesserNumber, GameWeekId = 22, Name = "Oli McBurnie"},
                new GoalScorers() { Id = 223, SeasonGuessId = guesserNumber, GameWeekId = 23, Name = "Steven Bergwijn"},
                new GoalScorers() { Id = 224, SeasonGuessId = guesserNumber, GameWeekId = 24, Name = "Olivier Giroud"},
                new GoalScorers() { Id = 225, SeasonGuessId = guesserNumber, GameWeekId = 25, Name = "Divock Origi"},
                new GoalScorers() { Id = 226, SeasonGuessId = guesserNumber, GameWeekId = 26, Name = "Wilfried Zaha"},
                new GoalScorers() { Id = 227, SeasonGuessId = guesserNumber, GameWeekId = 27, Name = "Alex Oxlade-Chamberlain"},
                new GoalScorers() { Id = 228, SeasonGuessId = guesserNumber, GameWeekId = 28, Name = "Ayoze Perez"},
                new GoalScorers() { Id = 229, SeasonGuessId = guesserNumber, GameWeekId = 29, Name = "Virgil Van Dijk"},
                new GoalScorers() { Id = 230, SeasonGuessId = guesserNumber, GameWeekId = 30, Name = "Dominic Calvert-Lewin"},
                new GoalScorers() { Id = 231, SeasonGuessId = guesserNumber, GameWeekId = 31, Name = "Kai Havertz"},
                new GoalScorers() { Id = 232, SeasonGuessId = guesserNumber, GameWeekId = 32, Name = "Pierre-Emerick Aubameyang"},
                new GoalScorers() { Id = 233, SeasonGuessId = guesserNumber, GameWeekId = 33, Name = "Rodrigo"},
                new GoalScorers() { Id = 234, SeasonGuessId = guesserNumber, GameWeekId = 34, Name = "Thiago Silva"},
                new GoalScorers() { Id = 235, SeasonGuessId = guesserNumber, GameWeekId = 35, Name = "Nicolas Pepe"},
                new GoalScorers() { Id = 236, SeasonGuessId = guesserNumber, GameWeekId = 36, Name = "Paul Pogba"},
                new GoalScorers() { Id = 237, SeasonGuessId = guesserNumber, GameWeekId = 37, Name = "Marcus Rashford"},
                new GoalScorers() { Id = 238, SeasonGuessId = guesserNumber, GameWeekId = 38, Name = "Patrick Bamford"}
            );
        }

        private static void AddIvar(ModelBuilder modelBuilder)
        {
            int guesserNumber = 3;


            modelBuilder.Entity<Guesser>().HasData(
                new Guesser(){ Id = guesserNumber, Name = "Ivar"}
            );

            modelBuilder.Entity<SeasonGuess>().HasData(
                new SeasonGuess(){ Id = guesserNumber, SeasonId = 1, GuesserId = guesserNumber}
            );

            modelBuilder.Entity<TopScorers>().HasData(
                new TopScorers() {
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    Scorer1 = "Aubameyang", Goals1 = 24,
                    Scorer2 = "Sterling", Goals2 = 22,
                    Scorer3 = "Salah", Goals3 =  20,
                    Scorer4 = "Mane", Goals4 = 18,
                    Scorer5 = "Aguero", Goals5 = 17
                }
            );

            modelBuilder.Entity<Table>().HasData(
                new Table(){
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    p1 = "12", p2 = "11", p3 = "13", p4 = "5", p5 = "17",
                    p6 = "1", p7 = "10", p8 = "20", p9 = "7", p10 = "4",
                    p11 = "9", p12 = "16", p13 = "6", p14 = "14", p15 = "19",
                    p16 = "2", p17 = "15", p18 = "3", p19 = "8", p20 = "18"
                }
            );

            modelBuilder.Entity<QuestionAnswer>().HasData(
                new QuestionAnswer(){ Id = 31, QuestionId = 1, SeasonGuessId = guesserNumber, Ans = "Roy Hodgson"},
                new QuestionAnswer(){ Id = 32, QuestionId = 2, SeasonGuessId = guesserNumber, Ans = "November"},
                new QuestionAnswer(){ Id = 33, QuestionId = 3, SeasonGuessId = guesserNumber, Ans = "Tarkowski"},
                new QuestionAnswer(){ Id = 34, QuestionId = 4, SeasonGuessId = guesserNumber, Ans = "Burnley"},
                new QuestionAnswer(){ Id = 35, QuestionId = 5, SeasonGuessId = guesserNumber, Ans = "Fulham"},
                new QuestionAnswer(){ Id = 36, QuestionId = 6, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"},
                new QuestionAnswer(){ Id = 37, QuestionId = 7, SeasonGuessId = guesserNumber, Ans = "Soyuncu"},
                new QuestionAnswer(){ Id = 38, QuestionId = 8, SeasonGuessId = guesserNumber, Ans = "Over"},
                new QuestionAnswer(){ Id = 39, QuestionId = 9, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 310, QuestionId = 10, SeasonGuessId = guesserNumber, Ans = "Ja"},
                new QuestionAnswer(){ Id = 311, QuestionId = 11, SeasonGuessId = guesserNumber, Ans = "Allison"},
                new QuestionAnswer(){ Id = 312, QuestionId = 12, SeasonGuessId = guesserNumber, Ans = "37"},
                new QuestionAnswer(){ Id = 313, QuestionId = 13, SeasonGuessId = guesserNumber, Ans = "Barnes"},
                new QuestionAnswer(){ Id = 314, QuestionId = 14, SeasonGuessId = guesserNumber, Ans = "Wesley"},
                new QuestionAnswer(){ Id = 315, QuestionId = 15, SeasonGuessId = guesserNumber, Ans = "Chelsea"},
                new QuestionAnswer(){ Id = 316, QuestionId = 16, SeasonGuessId = guesserNumber, Ans = "Tottenham"},
                new QuestionAnswer(){ Id = 317, QuestionId = 17, SeasonGuessId = guesserNumber, Ans = "Aubameyang"}
            );

            modelBuilder.Entity<MatchGuess>().HasData(
                new MatchGuess() { Id = 31, SeasonGuessId = guesserNumber, GameWeekId = 1, Res = 'h'},
                new MatchGuess() { Id = 32, SeasonGuessId = guesserNumber, GameWeekId = 2, Res = 'b'},
                new MatchGuess() { Id = 33, SeasonGuessId = guesserNumber, GameWeekId = 3, Res = 'h'},
                new MatchGuess() { Id = 34, SeasonGuessId = guesserNumber, GameWeekId = 4, Res = 'h'},
                new MatchGuess() { Id = 35, SeasonGuessId = guesserNumber, GameWeekId = 5, Res = 'u'},
                new MatchGuess() { Id = 36, SeasonGuessId = guesserNumber, GameWeekId = 6, Res = 'h'},
                new MatchGuess() { Id = 37, SeasonGuessId = guesserNumber, GameWeekId = 7, Res = 'h'},
                new MatchGuess() { Id = 38, SeasonGuessId = guesserNumber, GameWeekId = 8, Res = 'h'},
                new MatchGuess() { Id = 39, SeasonGuessId = guesserNumber, GameWeekId = 9, Res = 'b'},
                new MatchGuess() { Id = 310, SeasonGuessId = guesserNumber, GameWeekId = 10, Res = 'h'},
                new MatchGuess() { Id = 311, SeasonGuessId = guesserNumber, GameWeekId = 11, Res = 'b'},
                new MatchGuess() { Id = 312, SeasonGuessId = guesserNumber, GameWeekId = 12, Res = 'b'},
                new MatchGuess() { Id = 313, SeasonGuessId = guesserNumber, GameWeekId = 13, Res = 'h'},
                new MatchGuess() { Id = 314, SeasonGuessId = guesserNumber, GameWeekId = 14, Res = 'u'},
                new MatchGuess() { Id = 315, SeasonGuessId = guesserNumber, GameWeekId = 15, Res = 'u'},
                new MatchGuess() { Id = 316, SeasonGuessId = guesserNumber, GameWeekId = 16, Res = 'b'},
                new MatchGuess() { Id = 317, SeasonGuessId = guesserNumber, GameWeekId = 17, Res = 'b'},
                new MatchGuess() { Id = 318, SeasonGuessId = guesserNumber, GameWeekId = 18, Res = 'h'},
                new MatchGuess() { Id = 319, SeasonGuessId = guesserNumber, GameWeekId = 19, Res = 'b'},
                new MatchGuess() { Id = 320, SeasonGuessId = guesserNumber, GameWeekId = 20, Res = 'h'},
                new MatchGuess() { Id = 321, SeasonGuessId = guesserNumber, GameWeekId = 21, Res = 'b'},
                new MatchGuess() { Id = 322, SeasonGuessId = guesserNumber, GameWeekId = 22, Res = 'h'},
                new MatchGuess() { Id = 323, SeasonGuessId = guesserNumber, GameWeekId = 23, Res = 'h'},
                new MatchGuess() { Id = 324, SeasonGuessId = guesserNumber, GameWeekId = 24, Res = 'b'},
                new MatchGuess() { Id = 325, SeasonGuessId = guesserNumber, GameWeekId = 25, Res = 'h'},
                new MatchGuess() { Id = 326, SeasonGuessId = guesserNumber, GameWeekId = 26, Res = 'b'},
                new MatchGuess() { Id = 327, SeasonGuessId = guesserNumber, GameWeekId = 27, Res = 'b'},
                new MatchGuess() { Id = 328, SeasonGuessId = guesserNumber, GameWeekId = 28, Res = 'u'},
                new MatchGuess() { Id = 329, SeasonGuessId = guesserNumber, GameWeekId = 29, Res = 'b'},
                new MatchGuess() { Id = 330, SeasonGuessId = guesserNumber, GameWeekId = 30, Res = 'u'},
                new MatchGuess() { Id = 331, SeasonGuessId = guesserNumber, GameWeekId = 31, Res = 'b'},
                new MatchGuess() { Id = 332, SeasonGuessId = guesserNumber, GameWeekId = 32, Res = 'b'},
                new MatchGuess() { Id = 333, SeasonGuessId = guesserNumber, GameWeekId = 33, Res = 'h'},
                new MatchGuess() { Id = 334, SeasonGuessId = guesserNumber, GameWeekId = 34, Res = 'b'},
                new MatchGuess() { Id = 335, SeasonGuessId = guesserNumber, GameWeekId = 35, Res = 'b'},
                new MatchGuess() { Id = 336, SeasonGuessId = guesserNumber, GameWeekId = 36, Res = 'h'},
                new MatchGuess() { Id = 337, SeasonGuessId = guesserNumber, GameWeekId = 37, Res = 'b'},
                new MatchGuess() { Id = 338, SeasonGuessId = guesserNumber, GameWeekId = 38, Res = 'b'}
            );

            modelBuilder.Entity<GoalScorers>().HasData(
                new GoalScorers() { Id = 31, SeasonGuessId = guesserNumber, GameWeekId = 1, Name = "Aubameyang"},
                new GoalScorers() { Id = 32, SeasonGuessId = guesserNumber, GameWeekId = 2, Name = "Rashford"},
                new GoalScorers() { Id = 33, SeasonGuessId = guesserNumber, GameWeekId = 3, Name = "Kane"},
                new GoalScorers() { Id = 34, SeasonGuessId = guesserNumber, GameWeekId = 4, Name = "Werner"},
                new GoalScorers() { Id = 35, SeasonGuessId = guesserNumber, GameWeekId = 5, Name = "Vardy"},
                new GoalScorers() { Id = 36, SeasonGuessId = guesserNumber, GameWeekId = 6, Name = "Mane"},
                new GoalScorers() { Id = 37, SeasonGuessId = guesserNumber, GameWeekId = 7, Name = "Salah"},
                new GoalScorers() { Id = 38, SeasonGuessId = guesserNumber, GameWeekId = 8, Name = "Son Heung-min"},
                new GoalScorers() { Id = 39, SeasonGuessId = guesserNumber, GameWeekId = 9, Name = "Martial"},
                new GoalScorers() { Id = 310, SeasonGuessId = guesserNumber, GameWeekId = 10, Name = "De Bruyne"},
                new GoalScorers() { Id = 311, SeasonGuessId = guesserNumber, GameWeekId = 11, Name = "Bruno Fernandes"},
                new GoalScorers() { Id = 312, SeasonGuessId = guesserNumber, GameWeekId = 12, Name = "Jimenez"},
                new GoalScorers() { Id = 313, SeasonGuessId = guesserNumber, GameWeekId = 13, Name = "Greenwood"},
                new GoalScorers() { Id = 314, SeasonGuessId = guesserNumber, GameWeekId = 14, Name = "Mahrez"},
                new GoalScorers() { Id = 315, SeasonGuessId = guesserNumber, GameWeekId = 15, Name = "Sterling"},
                new GoalScorers() { Id = 316, SeasonGuessId = guesserNumber, GameWeekId = 16, Name = "Firmino"},
                new GoalScorers() { Id = 317, SeasonGuessId = guesserNumber, GameWeekId = 17, Name = "Calvert Lewin"},
                new GoalScorers() { Id = 318, SeasonGuessId = guesserNumber, GameWeekId = 18, Name = "Dele Alli"},
                new GoalScorers() { Id = 319, SeasonGuessId = guesserNumber, GameWeekId = 19, Name = "Bergwijn"},
                new GoalScorers() { Id = 320, SeasonGuessId = guesserNumber, GameWeekId = 20, Name = "Aguero"},
                new GoalScorers() { Id = 321, SeasonGuessId = guesserNumber, GameWeekId = 21, Name = "Willian"},
                new GoalScorers() { Id = 322, SeasonGuessId = guesserNumber, GameWeekId = 22, Name = "Barnes"},
                new GoalScorers() { Id = 323, SeasonGuessId = guesserNumber, GameWeekId = 23, Name = "Saka"},
                new GoalScorers() { Id = 324, SeasonGuessId = guesserNumber, GameWeekId = 24, Name = "Pogba"},
                new GoalScorers() { Id = 325, SeasonGuessId = guesserNumber, GameWeekId = 25, Name = "Van de Beek"},
                new GoalScorers() { Id = 326, SeasonGuessId = guesserNumber, GameWeekId = 26, Name = "Callum Wilson"},
                new GoalScorers() { Id = 327, SeasonGuessId = guesserNumber, GameWeekId = 27, Name = "Ings"},
                new GoalScorers() { Id = 328, SeasonGuessId = guesserNumber, GameWeekId = 28, Name = "Richarlison"},
                new GoalScorers() { Id = 329, SeasonGuessId = guesserNumber, GameWeekId = 29, Name = "Mitrovic"},
                new GoalScorers() { Id = 330, SeasonGuessId = guesserNumber, GameWeekId = 30, Name = "Batshuayi"},
                new GoalScorers() { Id = 331, SeasonGuessId = guesserNumber, GameWeekId = 31, Name = "Naby Keita"},
                new GoalScorers() { Id = 332, SeasonGuessId = guesserNumber, GameWeekId = 32, Name = "Winjaldum"},
                new GoalScorers() { Id = 333, SeasonGuessId = guesserNumber, GameWeekId = 33, Name = "Gabriel jesus"},
                new GoalScorers() { Id = 334, SeasonGuessId = guesserNumber, GameWeekId = 34, Name = "James Rodriguez"},
                new GoalScorers() { Id = 335, SeasonGuessId = guesserNumber, GameWeekId = 35, Name = "Maddison"},
                new GoalScorers() { Id = 336, SeasonGuessId = guesserNumber, GameWeekId = 36, Name = "Adama Traore"},
                new GoalScorers() { Id = 337, SeasonGuessId = guesserNumber, GameWeekId = 37, Name = "Havertz"},
                new GoalScorers() { Id = 338, SeasonGuessId = guesserNumber, GameWeekId = 38, Name = "Chris Wood"}
            );
        }

        private static void AddMakke(ModelBuilder modelBuilder)
        {
            int guesserNumber = 4;


            modelBuilder.Entity<Guesser>().HasData(
                new Guesser(){ Id = guesserNumber, Name = "Makke"}
            );

            modelBuilder.Entity<SeasonGuess>().HasData(
                new SeasonGuess(){ Id = guesserNumber, SeasonId = 1, GuesserId = guesserNumber}
            );

            modelBuilder.Entity<TopScorers>().HasData(
                new TopScorers() {
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    Scorer1 = "Mane", Goals1 = 23,
                    Scorer2 = "Werner", Goals2 = 21,
                    Scorer3 = "Salah", Goals3 =  20,
                    Scorer4 = "Aubameyang", Goals4 = 20,
                    Scorer5 = "Sterling", Goals5 = 18
                }
            );

            modelBuilder.Entity<Table>().HasData(
                new Table(){
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    p1 = "11", p2 = "12", p3 = "5", p4 = "13", p5 = "1",
                    p6 = "10", p7 = "17", p8 = "20", p9 = "14", p10 = "7",
                    p11 = "6", p12 = "15", p13 = "19", p14 = "16", p15 = "9",
                    p16 = "8", p17 = "2", p18 = "18", p19 = "3", p20 = "4"
                }
            );

            modelBuilder.Entity<QuestionAnswer>().HasData(
                new QuestionAnswer(){ Id = 41, QuestionId = 1, SeasonGuessId = guesserNumber, Ans = "Sean Dyche"},
                new QuestionAnswer(){ Id = 42, QuestionId = 2, SeasonGuessId = guesserNumber, Ans = "Oktober"},
                new QuestionAnswer(){ Id = 43, QuestionId = 3, SeasonGuessId = guesserNumber, Ans = "David Luiz"},
                new QuestionAnswer(){ Id = 44, QuestionId = 4, SeasonGuessId = guesserNumber, Ans = "Arsenal"},
                new QuestionAnswer(){ Id = 45, QuestionId = 5, SeasonGuessId = guesserNumber, Ans = "Arsenal"},
                new QuestionAnswer(){ Id = 46, QuestionId = 6, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"},
                new QuestionAnswer(){ Id = 47, QuestionId = 7, SeasonGuessId = guesserNumber, Ans = "Jesse Lingaard"},
                new QuestionAnswer(){ Id = 48, QuestionId = 8, SeasonGuessId = guesserNumber, Ans = "Over"},
                new QuestionAnswer(){ Id = 49, QuestionId = 9, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 410, QuestionId = 10, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 411, QuestionId = 11, SeasonGuessId = guesserNumber, Ans = "Allison"},
                new QuestionAnswer(){ Id = 412, QuestionId = 12, SeasonGuessId = guesserNumber, Ans = "34"},
                new QuestionAnswer(){ Id = 413, QuestionId = 13, SeasonGuessId = guesserNumber, Ans = "Barnes"},
                new QuestionAnswer(){ Id = 414, QuestionId = 14, SeasonGuessId = guesserNumber, Ans = "Iheanacho"},
                new QuestionAnswer(){ Id = 415, QuestionId = 15, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 416, QuestionId = 16, SeasonGuessId = guesserNumber, Ans = "Liverpool"},
                new QuestionAnswer(){ Id = 417, QuestionId = 17, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"}
            );

            modelBuilder.Entity<MatchGuess>().HasData(
                new MatchGuess() { Id = 41, SeasonGuessId = guesserNumber, GameWeekId = 1, Res = 'h'},
                new MatchGuess() { Id = 42, SeasonGuessId = guesserNumber, GameWeekId = 2, Res = 'u'},
                new MatchGuess() { Id = 43, SeasonGuessId = guesserNumber, GameWeekId = 3, Res = 'b'},
                new MatchGuess() { Id = 44, SeasonGuessId = guesserNumber, GameWeekId = 4, Res = 'h'},
                new MatchGuess() { Id = 45, SeasonGuessId = guesserNumber, GameWeekId = 5, Res = 'h'},
                new MatchGuess() { Id = 46, SeasonGuessId = guesserNumber, GameWeekId = 6, Res = 'b'},
                new MatchGuess() { Id = 47, SeasonGuessId = guesserNumber, GameWeekId = 7, Res = 'h'},
                new MatchGuess() { Id = 48, SeasonGuessId = guesserNumber, GameWeekId = 8, Res = 'h'},
                new MatchGuess() { Id = 49, SeasonGuessId = guesserNumber, GameWeekId = 9, Res = 'u'},
                new MatchGuess() { Id = 410, SeasonGuessId = guesserNumber, GameWeekId = 10, Res = 'h'},
                new MatchGuess() { Id = 411, SeasonGuessId = guesserNumber, GameWeekId = 11, Res = 'b'},
                new MatchGuess() { Id = 412, SeasonGuessId = guesserNumber, GameWeekId = 12, Res = 'b'},
                new MatchGuess() { Id = 413, SeasonGuessId = guesserNumber, GameWeekId = 13, Res = 'h'},
                new MatchGuess() { Id = 414, SeasonGuessId = guesserNumber, GameWeekId = 14, Res = 'b'},
                new MatchGuess() { Id = 415, SeasonGuessId = guesserNumber, GameWeekId = 15, Res = 'b'},
                new MatchGuess() { Id = 416, SeasonGuessId = guesserNumber, GameWeekId = 16, Res = 'u'},
                new MatchGuess() { Id = 417, SeasonGuessId = guesserNumber, GameWeekId = 17, Res = 'b'},
                new MatchGuess() { Id = 418, SeasonGuessId = guesserNumber, GameWeekId = 18, Res = 'h'},
                new MatchGuess() { Id = 419, SeasonGuessId = guesserNumber, GameWeekId = 19, Res = 'h'},
                new MatchGuess() { Id = 420, SeasonGuessId = guesserNumber, GameWeekId = 20, Res = 'b'},
                new MatchGuess() { Id = 421, SeasonGuessId = guesserNumber, GameWeekId = 21, Res = 'h'},
                new MatchGuess() { Id = 422, SeasonGuessId = guesserNumber, GameWeekId = 22, Res = 'b'},
                new MatchGuess() { Id = 423, SeasonGuessId = guesserNumber, GameWeekId = 23, Res = 'h'},
                new MatchGuess() { Id = 424, SeasonGuessId = guesserNumber, GameWeekId = 24, Res = 'u'},
                new MatchGuess() { Id = 425, SeasonGuessId = guesserNumber, GameWeekId = 25, Res = 'b'},
                new MatchGuess() { Id = 426, SeasonGuessId = guesserNumber, GameWeekId = 26, Res = 'h'},
                new MatchGuess() { Id = 427, SeasonGuessId = guesserNumber, GameWeekId = 27, Res = 'b'},
                new MatchGuess() { Id = 428, SeasonGuessId = guesserNumber, GameWeekId = 28, Res = 'h'},
                new MatchGuess() { Id = 429, SeasonGuessId = guesserNumber, GameWeekId = 29, Res = 'b'},
                new MatchGuess() { Id = 430, SeasonGuessId = guesserNumber, GameWeekId = 30, Res = 'h'},
                new MatchGuess() { Id = 431, SeasonGuessId = guesserNumber, GameWeekId = 31, Res = 'b'},
                new MatchGuess() { Id = 432, SeasonGuessId = guesserNumber, GameWeekId = 32, Res = 'b'},
                new MatchGuess() { Id = 433, SeasonGuessId = guesserNumber, GameWeekId = 33, Res = 'h'},
                new MatchGuess() { Id = 434, SeasonGuessId = guesserNumber, GameWeekId = 34, Res = 'b'},
                new MatchGuess() { Id = 435, SeasonGuessId = guesserNumber, GameWeekId = 35, Res = 'u'},
                new MatchGuess() { Id = 436, SeasonGuessId = guesserNumber, GameWeekId = 36, Res = 'u'},
                new MatchGuess() { Id = 437, SeasonGuessId = guesserNumber, GameWeekId = 37, Res = 'u'},
                new MatchGuess() { Id = 438, SeasonGuessId = guesserNumber, GameWeekId = 38, Res = 'h'}
            );

            modelBuilder.Entity<GoalScorers>().HasData(
                new GoalScorers() { Id = 41, SeasonGuessId = guesserNumber, GameWeekId = 1, Name = "Salah"},
                new GoalScorers() { Id = 42, SeasonGuessId = guesserNumber, GameWeekId = 2, Name = "Aubameyang"},
                new GoalScorers() { Id = 43, SeasonGuessId = guesserNumber, GameWeekId = 3, Name = "Rashford"},
                new GoalScorers() { Id = 44, SeasonGuessId = guesserNumber, GameWeekId = 4, Name = "Sterling"},
                new GoalScorers() { Id = 45, SeasonGuessId = guesserNumber, GameWeekId = 5, Name = "Kane"},
                new GoalScorers() { Id = 46, SeasonGuessId = guesserNumber, GameWeekId = 6, Name = "Werner"},
                new GoalScorers() { Id = 47, SeasonGuessId = guesserNumber, GameWeekId = 7, Name = "Martial"},
                new GoalScorers() { Id = 48, SeasonGuessId = guesserNumber, GameWeekId = 8, Name = "Vardy"},
                new GoalScorers() { Id = 49, SeasonGuessId = guesserNumber, GameWeekId = 9, Name = "Mane"},
                new GoalScorers() { Id = 410, SeasonGuessId = guesserNumber, GameWeekId = 10, Name = "Woods"},
                new GoalScorers() { Id = 411, SeasonGuessId = guesserNumber, GameWeekId = 11, Name = "Aguero"},
                new GoalScorers() { Id = 412, SeasonGuessId = guesserNumber, GameWeekId = 12, Name = "Jesus"},
                new GoalScorers() { Id = 413, SeasonGuessId = guesserNumber, GameWeekId = 13, Name = "Zieich "},
                new GoalScorers() { Id = 414, SeasonGuessId = guesserNumber, GameWeekId = 14, Name = "Firminio"},
                new GoalScorers() { Id = 415, SeasonGuessId = guesserNumber, GameWeekId = 15, Name = "Greenwood"},
                new GoalScorers() { Id = 416, SeasonGuessId = guesserNumber, GameWeekId = 16, Name = "Hueng min Son"},
                new GoalScorers() { Id = 417, SeasonGuessId = guesserNumber, GameWeekId = 17, Name = "Van Dijk"},
                new GoalScorers() { Id = 418, SeasonGuessId = guesserNumber, GameWeekId = 18, Name = "Henderson"},
                new GoalScorers() { Id = 419, SeasonGuessId = guesserNumber, GameWeekId = 19, Name = "Ings"},
                new GoalScorers() { Id = 420, SeasonGuessId = guesserNumber, GameWeekId = 20, Name = "Bergwijn"},
                new GoalScorers() { Id = 421, SeasonGuessId = guesserNumber, GameWeekId = 21, Name = "Mings"},
                new GoalScorers() { Id = 422, SeasonGuessId = guesserNumber, GameWeekId = 22, Name = "Mahrez"},
                new GoalScorers() { Id = 423, SeasonGuessId = guesserNumber, GameWeekId = 23, Name = "Kevin De Bruyne"},
                new GoalScorers() { Id = 424, SeasonGuessId = guesserNumber, GameWeekId = 24, Name = "Pogba"},
                new GoalScorers() { Id = 425, SeasonGuessId = guesserNumber, GameWeekId = 25, Name = "Bruno Fernandes"},
                new GoalScorers() { Id = 426, SeasonGuessId = guesserNumber, GameWeekId = 26, Name = "Pulisic"},
                new GoalScorers() { Id = 427, SeasonGuessId = guesserNumber, GameWeekId = 27, Name = "Willian"},
                new GoalScorers() { Id = 428, SeasonGuessId = guesserNumber, GameWeekId = 28, Name = "Nicolas Pepe"},
                new GoalScorers() { Id = 429, SeasonGuessId = guesserNumber, GameWeekId = 29, Name = "Saka"},
                new GoalScorers() { Id = 430, SeasonGuessId = guesserNumber, GameWeekId = 30, Name = "Alexander Arnold"},
                new GoalScorers() { Id = 431, SeasonGuessId = guesserNumber, GameWeekId = 31, Name = "Robertson"},
                new GoalScorers() { Id = 432, SeasonGuessId = guesserNumber, GameWeekId = 32, Name = "Richarlison "},
                new GoalScorers() { Id = 433, SeasonGuessId = guesserNumber, GameWeekId = 33, Name = "James Rodriguez"},
                new GoalScorers() { Id = 434, SeasonGuessId = guesserNumber, GameWeekId = 34, Name = "Rodrigo"},
                new GoalScorers() { Id = 435, SeasonGuessId = guesserNumber, GameWeekId = 35, Name = "Laporte"},
                new GoalScorers() { Id = 436, SeasonGuessId = guesserNumber, GameWeekId = 36, Name = "Dele Alli"},
                new GoalScorers() { Id = 437, SeasonGuessId = guesserNumber, GameWeekId = 37, Name = "Bernardo Silva"},
                new GoalScorers() { Id = 438, SeasonGuessId = guesserNumber, GameWeekId = 38, Name = "Kante"}
            );
        }

        private static void AddMarty(ModelBuilder modelBuilder)
        {
             int guesserNumber = 5;


            modelBuilder.Entity<Guesser>().HasData(
                new Guesser(){ Id = guesserNumber, Name = "Marty"}
            );

            modelBuilder.Entity<SeasonGuess>().HasData(
                new SeasonGuess(){ Id = guesserNumber, SeasonId = 1, GuesserId = guesserNumber}
            );

            modelBuilder.Entity<TopScorers>().HasData(
                new TopScorers() {
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    Scorer1 = "Salah", Goals1 = 28,
                    Scorer2 = "Aubameyang", Goals2 = 26,
                    Scorer3 = "Kane", Goals3 =  26,
                    Scorer4 = "Sterling", Goals4 = 24,
                    Scorer5 = "Mane", Goals5 = 22
                }
            );

            modelBuilder.Entity<Table>().HasData(
                new Table(){
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    p1 = "11", p2 = "12", p3 = "13", p4 = "5", p5 = "17",
                    p6 = "1", p7 = "20", p8 = "10", p9 = "7", p10 = "9",
                    p11 = "4", p12 = "16", p13 = "15", p14 = "6", p15 = "14",
                    p16 = "19", p17 = "2", p18 = "3", p19 = "18", p20 = "8"
                }
            );

            modelBuilder.Entity<QuestionAnswer>().HasData(
                new QuestionAnswer(){ Id = 51, QuestionId = 1, SeasonGuessId = guesserNumber, Ans = "David Moyes"},
                new QuestionAnswer(){ Id = 52, QuestionId = 2, SeasonGuessId = guesserNumber, Ans = "November"},
                new QuestionAnswer(){ Id = 53, QuestionId = 3, SeasonGuessId = guesserNumber, Ans = "Luka Milivojevic"},
                new QuestionAnswer(){ Id = 54, QuestionId = 4, SeasonGuessId = guesserNumber, Ans = "Tottenham Hotspur"},
                new QuestionAnswer(){ Id = 55, QuestionId = 5, SeasonGuessId = guesserNumber, Ans = "Arsenal"},
                new QuestionAnswer(){ Id = 56, QuestionId = 6, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"},
                new QuestionAnswer(){ Id = 57, QuestionId = 7, SeasonGuessId = guesserNumber, Ans = "Lindeløf"},
                new QuestionAnswer(){ Id = 58, QuestionId = 8, SeasonGuessId = guesserNumber, Ans = "Over"},
                new QuestionAnswer(){ Id = 59, QuestionId = 9, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 510, QuestionId = 10, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 511, QuestionId = 11, SeasonGuessId = guesserNumber, Ans = "De Gea"},
                new QuestionAnswer(){ Id = 512, QuestionId = 12, SeasonGuessId = guesserNumber, Ans = "38"},
                new QuestionAnswer(){ Id = 513, QuestionId = 13, SeasonGuessId = guesserNumber, Ans = "Barnes"},
                new QuestionAnswer(){ Id = 514, QuestionId = 14, SeasonGuessId = guesserNumber, Ans = "Iheanacho"},
                new QuestionAnswer(){ Id = 515, QuestionId = 15, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 516, QuestionId = 16, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 517, QuestionId = 17, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"}
            );

            modelBuilder.Entity<MatchGuess>().HasData(
                new MatchGuess() { Id = 51, SeasonGuessId = guesserNumber, GameWeekId = 1, Res = 'b'},
                new MatchGuess() { Id = 52, SeasonGuessId = guesserNumber, GameWeekId = 2, Res = 'b'},
                new MatchGuess() { Id = 53, SeasonGuessId = guesserNumber, GameWeekId = 3, Res = 'b'},
                new MatchGuess() { Id = 54, SeasonGuessId = guesserNumber, GameWeekId = 4, Res = 'h'},
                new MatchGuess() { Id = 55, SeasonGuessId = guesserNumber, GameWeekId = 5, Res = 'h'},
                new MatchGuess() { Id = 56, SeasonGuessId = guesserNumber, GameWeekId = 6, Res = 'h'},
                new MatchGuess() { Id = 57, SeasonGuessId = guesserNumber, GameWeekId = 7, Res = 'h'},
                new MatchGuess() { Id = 58, SeasonGuessId = guesserNumber, GameWeekId = 8, Res = 'b'},
                new MatchGuess() { Id = 59, SeasonGuessId = guesserNumber, GameWeekId = 9, Res = 'u'},
                new MatchGuess() { Id = 510, SeasonGuessId = guesserNumber, GameWeekId = 10, Res = 'h'},
                new MatchGuess() { Id = 511, SeasonGuessId = guesserNumber, GameWeekId = 11, Res = 'h'},
                new MatchGuess() { Id = 512, SeasonGuessId = guesserNumber, GameWeekId = 12, Res = 'u'},
                new MatchGuess() { Id = 513, SeasonGuessId = guesserNumber, GameWeekId = 13, Res = 'h'},
                new MatchGuess() { Id = 514, SeasonGuessId = guesserNumber, GameWeekId = 14, Res = 'u'},
                new MatchGuess() { Id = 515, SeasonGuessId = guesserNumber, GameWeekId = 15, Res = 'b'},
                new MatchGuess() { Id = 516, SeasonGuessId = guesserNumber, GameWeekId = 16, Res = 'b'},
                new MatchGuess() { Id = 517, SeasonGuessId = guesserNumber, GameWeekId = 17, Res = 'b'},
                new MatchGuess() { Id = 518, SeasonGuessId = guesserNumber, GameWeekId = 18, Res = 'h'},
                new MatchGuess() { Id = 519, SeasonGuessId = guesserNumber, GameWeekId = 19, Res = 'b'},
                new MatchGuess() { Id = 520, SeasonGuessId = guesserNumber, GameWeekId = 20, Res = 'b'},
                new MatchGuess() { Id = 521, SeasonGuessId = guesserNumber, GameWeekId = 21, Res = 'u'},
                new MatchGuess() { Id = 522, SeasonGuessId = guesserNumber, GameWeekId = 22, Res = 'b'},
                new MatchGuess() { Id = 523, SeasonGuessId = guesserNumber, GameWeekId = 23, Res = 'h'},
                new MatchGuess() { Id = 524, SeasonGuessId = guesserNumber, GameWeekId = 24, Res = 'h'},
                new MatchGuess() { Id = 525, SeasonGuessId = guesserNumber, GameWeekId = 25, Res = 'b'},
                new MatchGuess() { Id = 526, SeasonGuessId = guesserNumber, GameWeekId = 26, Res = 'b'},
                new MatchGuess() { Id = 527, SeasonGuessId = guesserNumber, GameWeekId = 27, Res = 'b'},
                new MatchGuess() { Id = 528, SeasonGuessId = guesserNumber, GameWeekId = 28, Res = 'u'},
                new MatchGuess() { Id = 529, SeasonGuessId = guesserNumber, GameWeekId = 29, Res = 'b'},
                new MatchGuess() { Id = 530, SeasonGuessId = guesserNumber, GameWeekId = 30, Res = 'h'},
                new MatchGuess() { Id = 531, SeasonGuessId = guesserNumber, GameWeekId = 31, Res = 'b'},
                new MatchGuess() { Id = 532, SeasonGuessId = guesserNumber, GameWeekId = 32, Res = 'b'},
                new MatchGuess() { Id = 533, SeasonGuessId = guesserNumber, GameWeekId = 33, Res = 'h'},
                new MatchGuess() { Id = 534, SeasonGuessId = guesserNumber, GameWeekId = 34, Res = 'b'},
                new MatchGuess() { Id = 535, SeasonGuessId = guesserNumber, GameWeekId = 35, Res = 'b'},
                new MatchGuess() { Id = 536, SeasonGuessId = guesserNumber, GameWeekId = 36, Res = 'h'},
                new MatchGuess() { Id = 537, SeasonGuessId = guesserNumber, GameWeekId = 37, Res = 'b'},
                new MatchGuess() { Id = 538, SeasonGuessId = guesserNumber, GameWeekId = 38, Res = 'b'}
            );

            modelBuilder.Entity<GoalScorers>().HasData(
                new GoalScorers() { Id = 51, SeasonGuessId = guesserNumber, GameWeekId = 1, Name = "Salah"},
                new GoalScorers() { Id = 52, SeasonGuessId = guesserNumber, GameWeekId = 2, Name = "Calvert Lewin"},
                new GoalScorers() { Id = 53, SeasonGuessId = guesserNumber, GameWeekId = 3, Name = "Kane"},
                new GoalScorers() { Id = 54, SeasonGuessId = guesserNumber, GameWeekId = 4, Name = "Raúl Jiménez"},
                new GoalScorers() { Id = 55, SeasonGuessId = guesserNumber, GameWeekId = 5, Name = "Vardy"},
                new GoalScorers() { Id = 56, SeasonGuessId = guesserNumber, GameWeekId = 6, Name = "Sterling"},
                new GoalScorers() { Id = 57, SeasonGuessId = guesserNumber, GameWeekId = 7, Name = "Heung Min Son"},
                new GoalScorers() { Id = 58, SeasonGuessId = guesserNumber, GameWeekId = 8, Name = "Pepe"},
                new GoalScorers() { Id = 59, SeasonGuessId = guesserNumber, GameWeekId = 9, Name = "Martial"},
                new GoalScorers() { Id = 510, SeasonGuessId = guesserNumber, GameWeekId = 10, Name = "Perez"},
                new GoalScorers() { Id = 511, SeasonGuessId = guesserNumber, GameWeekId = 11, Name = "Werner"},
                new GoalScorers() { Id = 512, SeasonGuessId = guesserNumber, GameWeekId = 12, Name = "Barnes"},
                new GoalScorers() { Id = 513, SeasonGuessId = guesserNumber, GameWeekId = 13, Name = "Kevin de bruyne"},
                new GoalScorers() { Id = 514, SeasonGuessId = guesserNumber, GameWeekId = 14, Name = "Pulisic"},
                new GoalScorers() { Id = 515, SeasonGuessId = guesserNumber, GameWeekId = 15, Name = "Trezeguet"},
                new GoalScorers() { Id = 516, SeasonGuessId = guesserNumber, GameWeekId = 16, Name = "Alli"},
                new GoalScorers() { Id = 517, SeasonGuessId = guesserNumber, GameWeekId = 17, Name = "Willian"},
                new GoalScorers() { Id = 518, SeasonGuessId = guesserNumber, GameWeekId = 18, Name = "Mahrez"},
                new GoalScorers() { Id = 519, SeasonGuessId = guesserNumber, GameWeekId = 19, Name = "Jota"},
                new GoalScorers() { Id = 520, SeasonGuessId = guesserNumber, GameWeekId = 20, Name = "Jesus"},
                new GoalScorers() { Id = 521, SeasonGuessId = guesserNumber, GameWeekId = 21, Name = "Bergwijn"},
                new GoalScorers() { Id = 522, SeasonGuessId = guesserNumber, GameWeekId = 22, Name = "Mane"},
                new GoalScorers() { Id = 523, SeasonGuessId = guesserNumber, GameWeekId = 23, Name = "Moura"},
                new GoalScorers() { Id = 524, SeasonGuessId = guesserNumber, GameWeekId = 24, Name = "Zyiech"},
                new GoalScorers() { Id = 525, SeasonGuessId = guesserNumber, GameWeekId = 25, Name = "Rashford"},
                new GoalScorers() { Id = 526, SeasonGuessId = guesserNumber, GameWeekId = 26, Name = "Bernardo Silva"},
                new GoalScorers() { Id = 527, SeasonGuessId = guesserNumber, GameWeekId = 27, Name = "Pogba"},
                new GoalScorers() { Id = 528, SeasonGuessId = guesserNumber, GameWeekId = 28, Name = "Ings"},
                new GoalScorers() { Id = 529, SeasonGuessId = guesserNumber, GameWeekId = 29, Name = "Rodrigo"},
                new GoalScorers() { Id = 530, SeasonGuessId = guesserNumber, GameWeekId = 30, Name = "Bruno Fernandes"},
                new GoalScorers() { Id = 531, SeasonGuessId = guesserNumber, GameWeekId = 31, Name = "Aguero"},
                new GoalScorers() { Id = 532, SeasonGuessId = guesserNumber, GameWeekId = 32, Name = "Iheanacho"},
                new GoalScorers() { Id = 533, SeasonGuessId = guesserNumber, GameWeekId = 33, Name = "Grealish"},
                new GoalScorers() { Id = 534, SeasonGuessId = guesserNumber, GameWeekId = 34, Name = "Havertz"},
                new GoalScorers() { Id = 535, SeasonGuessId = guesserNumber, GameWeekId = 35, Name = "Nketiah"},
                new GoalScorers() { Id = 536, SeasonGuessId = guesserNumber, GameWeekId = 36, Name = "Che Adams"},
                new GoalScorers() { Id = 537, SeasonGuessId = guesserNumber, GameWeekId = 37, Name = "Greenwood"},
                new GoalScorers() { Id = 538, SeasonGuessId = guesserNumber, GameWeekId = 38, Name = "Callum Wilson"}
            );
        }

        private static void AddBenny(ModelBuilder modelBuilder)
        {
            int guesserNumber = 6;


            modelBuilder.Entity<Guesser>().HasData(
                new Guesser(){ Id = guesserNumber, Name = "Benny"}
            );

            modelBuilder.Entity<SeasonGuess>().HasData(
                new SeasonGuess(){ Id = guesserNumber, SeasonId = 1, GuesserId = guesserNumber}
            );

            modelBuilder.Entity<TopScorers>().HasData(
                new TopScorers() {
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    Scorer1 = "Kane", Goals1 = 26,
                    Scorer2 = "Salah", Goals2 = 23,
                    Scorer3 = "Werner", Goals3 =  21,
                    Scorer4 = "Aubemeyang", Goals4 = 21,
                    Scorer5 = "Rashford", Goals5 = 19
                }
            );

            modelBuilder.Entity<Table>().HasData(
                new Table(){
                    Id = guesserNumber, SeasonGuessId = guesserNumber,
                    p1 = "12", p2 = "5", p3 = "11", p4 = "13", p5 = "17",
                    p6 = "1", p7 = "7", p8 = "10", p9 = "20", p10 = "9",
                    p11 = "16", p12 = "15", p13 = "6", p14 = "4", p15 = "14",
                    p16 = "8", p17 = "19", p18 = "2", p19 = "3", p20 = "8"
                }
            );

            modelBuilder.Entity<QuestionAnswer>().HasData(
                new QuestionAnswer(){ Id = 61, QuestionId = 1, SeasonGuessId = guesserNumber, Ans = "Dean Smith"},
                new QuestionAnswer(){ Id = 62, QuestionId = 2, SeasonGuessId = guesserNumber, Ans = "Oktober"},
                new QuestionAnswer(){ Id = 63, QuestionId = 3, SeasonGuessId = guesserNumber, Ans = "Westwood"},
                new QuestionAnswer(){ Id = 64, QuestionId = 4, SeasonGuessId = guesserNumber, Ans = "Burnley"},
                new QuestionAnswer(){ Id = 65, QuestionId = 5, SeasonGuessId = guesserNumber, Ans = "Brighton"},
                new QuestionAnswer(){ Id = 66, QuestionId = 6, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"},
                new QuestionAnswer(){ Id = 67, QuestionId = 7, SeasonGuessId = guesserNumber, Ans = "Reece James"},
                new QuestionAnswer(){ Id = 68, QuestionId = 8, SeasonGuessId = guesserNumber, Ans = "Over"},
                new QuestionAnswer(){ Id = 69, QuestionId = 9, SeasonGuessId = guesserNumber, Ans = "Ja"},
                new QuestionAnswer(){ Id = 610, QuestionId = 10, SeasonGuessId = guesserNumber, Ans = "Nei"},
                new QuestionAnswer(){ Id = 611, QuestionId = 11, SeasonGuessId = guesserNumber, Ans = "Ederson"},
                new QuestionAnswer(){ Id = 612, QuestionId = 12, SeasonGuessId = guesserNumber, Ans = "35"},
                new QuestionAnswer(){ Id = 613, QuestionId = 13, SeasonGuessId = guesserNumber, Ans = "Barnes"},
                new QuestionAnswer(){ Id = 614, QuestionId = 14, SeasonGuessId = guesserNumber, Ans = "Iheanacho"},
                new QuestionAnswer(){ Id = 615, QuestionId = 15, SeasonGuessId = guesserNumber, Ans = "Chelsea"},
                new QuestionAnswer(){ Id = 616, QuestionId = 16, SeasonGuessId = guesserNumber, Ans = "Manchester City"},
                new QuestionAnswer(){ Id = 617, QuestionId = 17, SeasonGuessId = guesserNumber, Ans = "Kevin De Bruyne"}
            );

            modelBuilder.Entity<MatchGuess>().HasData(
                new MatchGuess() { Id = 61, SeasonGuessId = guesserNumber, GameWeekId = 1, Res = 'h'},
                new MatchGuess() { Id = 62, SeasonGuessId = guesserNumber, GameWeekId = 2, Res = 'h'},
                new MatchGuess() { Id = 63, SeasonGuessId = guesserNumber, GameWeekId = 3, Res = 'u'},
                new MatchGuess() { Id = 64, SeasonGuessId = guesserNumber, GameWeekId = 4, Res = 'u'},
                new MatchGuess() { Id = 65, SeasonGuessId = guesserNumber, GameWeekId = 5, Res = 'h'},
                new MatchGuess() { Id = 66, SeasonGuessId = guesserNumber, GameWeekId = 6, Res = 'b'},
                new MatchGuess() { Id = 67, SeasonGuessId = guesserNumber, GameWeekId = 7, Res = 'h'},
                new MatchGuess() { Id = 68, SeasonGuessId = guesserNumber, GameWeekId = 8, Res = 'h'},
                new MatchGuess() { Id = 69, SeasonGuessId = guesserNumber, GameWeekId = 9, Res = 'h'},
                new MatchGuess() { Id = 610, SeasonGuessId = guesserNumber, GameWeekId = 10, Res = 'h'},
                new MatchGuess() { Id = 611, SeasonGuessId = guesserNumber, GameWeekId = 11, Res = 'h'},
                new MatchGuess() { Id = 612, SeasonGuessId = guesserNumber, GameWeekId = 12, Res = 'b'},
                new MatchGuess() { Id = 613, SeasonGuessId = guesserNumber, GameWeekId = 13, Res = 'h'},
                new MatchGuess() { Id = 614, SeasonGuessId = guesserNumber, GameWeekId = 14, Res = 'u'},
                new MatchGuess() { Id = 615, SeasonGuessId = guesserNumber, GameWeekId = 15, Res = 'b'},
                new MatchGuess() { Id = 616, SeasonGuessId = guesserNumber, GameWeekId = 16, Res = 'u'},
                new MatchGuess() { Id = 617, SeasonGuessId = guesserNumber, GameWeekId = 17, Res = 'h'},
                new MatchGuess() { Id = 618, SeasonGuessId = guesserNumber, GameWeekId = 18, Res = 'h'},
                new MatchGuess() { Id = 619, SeasonGuessId = guesserNumber, GameWeekId = 19, Res = 'u'},
                new MatchGuess() { Id = 620, SeasonGuessId = guesserNumber, GameWeekId = 20, Res = 'h'},
                new MatchGuess() { Id = 621, SeasonGuessId = guesserNumber, GameWeekId = 21, Res = 'h'},
                new MatchGuess() { Id = 622, SeasonGuessId = guesserNumber, GameWeekId = 22, Res = 'b'},
                new MatchGuess() { Id = 623, SeasonGuessId = guesserNumber, GameWeekId = 23, Res = 'u'},
                new MatchGuess() { Id = 624, SeasonGuessId = guesserNumber, GameWeekId = 24, Res = 'h'},
                new MatchGuess() { Id = 625, SeasonGuessId = guesserNumber, GameWeekId = 25, Res = 'u'},
                new MatchGuess() { Id = 626, SeasonGuessId = guesserNumber, GameWeekId = 26, Res = 'h'},
                new MatchGuess() { Id = 627, SeasonGuessId = guesserNumber, GameWeekId = 27, Res = 'h'},
                new MatchGuess() { Id = 628, SeasonGuessId = guesserNumber, GameWeekId = 28, Res = 'u'},
                new MatchGuess() { Id = 629, SeasonGuessId = guesserNumber, GameWeekId = 29, Res = 'b'},
                new MatchGuess() { Id = 630, SeasonGuessId = guesserNumber, GameWeekId = 30, Res = 'h'},
                new MatchGuess() { Id = 631, SeasonGuessId = guesserNumber, GameWeekId = 31, Res = 'b'},
                new MatchGuess() { Id = 632, SeasonGuessId = guesserNumber, GameWeekId = 32, Res = 'h'},
                new MatchGuess() { Id = 633, SeasonGuessId = guesserNumber, GameWeekId = 33, Res = 'h'},
                new MatchGuess() { Id = 634, SeasonGuessId = guesserNumber, GameWeekId = 34, Res = 'b'},
                new MatchGuess() { Id = 635, SeasonGuessId = guesserNumber, GameWeekId = 35, Res = 'b'},
                new MatchGuess() { Id = 636, SeasonGuessId = guesserNumber, GameWeekId = 36, Res = 'h'},
                new MatchGuess() { Id = 637, SeasonGuessId = guesserNumber, GameWeekId = 37, Res = 'h'},
                new MatchGuess() { Id = 638, SeasonGuessId = guesserNumber, GameWeekId = 38, Res = 'b'}
            );

            modelBuilder.Entity<GoalScorers>().HasData(
                new GoalScorers() { Id = 61, SeasonGuessId = guesserNumber, GameWeekId = 1, Name = "Werner"},
                new GoalScorers() { Id = 62, SeasonGuessId = guesserNumber, GameWeekId = 2, Name = "Aubameyang"},
                new GoalScorers() { Id = 63, SeasonGuessId = guesserNumber, GameWeekId = 3, Name = "Son"},
                new GoalScorers() { Id = 64, SeasonGuessId = guesserNumber, GameWeekId = 4, Name = "James Rodriguez"},
                new GoalScorers() { Id = 65, SeasonGuessId = guesserNumber, GameWeekId = 5, Name = "Kane"},
                new GoalScorers() { Id = 66, SeasonGuessId = guesserNumber, GameWeekId = 6, Name = "Salah"},
                new GoalScorers() { Id = 67, SeasonGuessId = guesserNumber, GameWeekId = 7, Name = "Mane"},
                new GoalScorers() { Id = 68, SeasonGuessId = guesserNumber, GameWeekId = 8, Name = "Havertz"},
                new GoalScorers() { Id = 69, SeasonGuessId = guesserNumber, GameWeekId = 9, Name = "Rashford"},
                new GoalScorers() { Id = 610, SeasonGuessId = guesserNumber, GameWeekId = 10, Name = "Vardy"},
                new GoalScorers() { Id = 611, SeasonGuessId = guesserNumber, GameWeekId = 11, Name = "De Bruyne"},
                new GoalScorers() { Id = 612, SeasonGuessId = guesserNumber, GameWeekId = 12, Name = "Willian"},
                new GoalScorers() { Id = 613, SeasonGuessId = guesserNumber, GameWeekId = 13, Name = "Sterling"},
                new GoalScorers() { Id = 614, SeasonGuessId = guesserNumber, GameWeekId = 14, Name = "Bruno Fernandes"},
                new GoalScorers() { Id = 615, SeasonGuessId = guesserNumber, GameWeekId = 15, Name = "Grealish"},
                new GoalScorers() { Id = 616, SeasonGuessId = guesserNumber, GameWeekId = 16, Name = "Wood"},
                new GoalScorers() { Id = 617, SeasonGuessId = guesserNumber, GameWeekId = 17, Name = "Martial"},
                new GoalScorers() { Id = 618, SeasonGuessId = guesserNumber, GameWeekId = 18, Name = "Gabriel Jesus"},
                new GoalScorers() { Id = 619, SeasonGuessId = guesserNumber, GameWeekId = 19, Name = "Rodrigo"},
                new GoalScorers() { Id = 620, SeasonGuessId = guesserNumber, GameWeekId = 20, Name = "Zaha"},
                new GoalScorers() { Id = 621, SeasonGuessId = guesserNumber, GameWeekId = 21, Name = "Aguero"},
                new GoalScorers() { Id = 622, SeasonGuessId = guesserNumber, GameWeekId = 22, Name = "Firmino"},
                new GoalScorers() { Id = 623, SeasonGuessId = guesserNumber, GameWeekId = 23, Name = "Bergwjin"},
                new GoalScorers() { Id = 624, SeasonGuessId = guesserNumber, GameWeekId = 24, Name = "Zieych"},
                new GoalScorers() { Id = 625, SeasonGuessId = guesserNumber, GameWeekId = 25, Name = "Greenwood"},
                new GoalScorers() { Id = 626, SeasonGuessId = guesserNumber, GameWeekId = 26, Name = "michy batshuayi"},
                new GoalScorers() { Id = 627, SeasonGuessId = guesserNumber, GameWeekId = 27, Name = "Alexander Arnold"},
                new GoalScorers() { Id = 628, SeasonGuessId = guesserNumber, GameWeekId = 28, Name = "Calvert-Levin"},
                new GoalScorers() { Id = 629, SeasonGuessId = guesserNumber, GameWeekId = 29, Name = "Mitrovic"},
                new GoalScorers() { Id = 630, SeasonGuessId = guesserNumber, GameWeekId = 30, Name = "Mount"},
                new GoalScorers() { Id = 631, SeasonGuessId = guesserNumber, GameWeekId = 31, Name = "Torres på city"},
                new GoalScorers() { Id = 632, SeasonGuessId = guesserNumber, GameWeekId = 32, Name = "Pogba"},
                new GoalScorers() { Id = 633, SeasonGuessId = guesserNumber, GameWeekId = 33, Name = "Mahrez"},
                new GoalScorers() { Id = 634, SeasonGuessId = guesserNumber, GameWeekId = 34, Name = "Pulisic"},
                new GoalScorers() { Id = 635, SeasonGuessId = guesserNumber, GameWeekId = 35, Name = "Barnes"},
                new GoalScorers() { Id = 636, SeasonGuessId = guesserNumber, GameWeekId = 36, Name = "Ings"},
                new GoalScorers() { Id = 637, SeasonGuessId = guesserNumber, GameWeekId = 37, Name = "Richarlison"},
                new GoalScorers() { Id = 638, SeasonGuessId = guesserNumber, GameWeekId = 38, Name = "Jimenez"}
            );
        }
    }
}