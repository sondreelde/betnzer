using Microsoft.EntityFrameworkCore;
using Betnzer.Data.Models;
using Betnzer.Data.Models.Guess;

namespace Betnzer.Data
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext() {}
        public ApplicationDBContext(DbContextOptions options): base(options) {}
        
        public DbSet<Guesser> Guessers { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<SeasonGuess> SeasonGuesses { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<GameWeek> GameWeeks { get; set; }
        public DbSet<TopScorers> TopScorers { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<QuestionAnswer> QuestionAnswers { get; set; }
        public DbSet<GoalScorers> GoalScorers { get; set; }
        public DbSet<MatchGuess> MatchGuesses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            ApplicationDBSeeder.Seed(modelBuilder);

        }
    }
}