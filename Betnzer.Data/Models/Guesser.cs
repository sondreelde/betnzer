using System.Collections.Generic;
using Betnzer.Data.Models.Guess;

namespace Betnzer.Data.Models
{
    public class Guesser
    {
        public int Id { get; set; } 
        public string Name { get; set; }

        // Guesses
        public ICollection<SeasonGuess> SeasonGuesses { get; set; }
    }
}