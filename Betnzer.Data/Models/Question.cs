using System.Collections.Generic;
using Betnzer.Data.Models.Guess;

namespace Betnzer.Data.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string Short { get; set; }

        public string Text { get; set; }

        public ICollection<QuestionAnswer> QuestionAnswers { get; set; }


        // Season
        public int SeasonId { get; set; }
        public Season Season { get; set; }
    }
}