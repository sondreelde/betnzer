using System.Collections.Generic;
using Betnzer.Data.Models.Guess;

namespace Betnzer.Data.Models
{
    public class Season
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // SeasonGuesses
        public ICollection<SeasonGuess> SeasonGuesses { get; set; }

        // Questions
        public ICollection<Question> Questions { get; set; }
        

    }
}