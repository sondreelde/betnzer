namespace Betnzer.Data.Models.Guess
{
    public class QuestionAnswer
    {
        public int Id { get; set; }

        public string Ans { get; set; }

        // Question
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        
        // SeasonGuess
        public int SeasonGuessId { get; set; }
        public SeasonGuess SeasonGuess { get; set; }
    }
}