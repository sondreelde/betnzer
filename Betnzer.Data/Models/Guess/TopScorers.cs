namespace Betnzer.Data.Models.Guess
{
    public class TopScorers
    {
        public int Id { get; set; }

        // List of topscorers
        public string Scorer1 { get; set; }
        public int Goals1 { get; set; }
        public string Scorer2 { get; set; }
        public int Goals2 { get; set; }
        public string Scorer3 { get; set; }
        public int Goals3 { get; set; }
        public string Scorer4 { get; set; }
        public int Goals4 { get; set; }
        public string Scorer5 { get; set; }
        public int Goals5 { get; set; }
        
        // SeasonGuess
        public int SeasonGuessId { get; set; }
        public SeasonGuess SeasonGuess { get; set; }
    }
}