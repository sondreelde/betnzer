namespace Betnzer.Data.Models.Guess
{
    public class GoalScorers
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // SeasonGuess
        public int SeasonGuessId { get; set; }
        public SeasonGuess SeasonGuess { get; set; }

        // GameWeek
        public int GameWeekId { get; set; }
        public GameWeek GameWeek { get; set; }

    }
}