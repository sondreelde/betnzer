namespace Betnzer.Data.Models.Guess
{
    public class MatchGuess
    {
        public int Id { get; set; }

        public char Res { get; set; }

        // GameWeek
        public int GameWeekId { get; set; }
        public GameWeek GameWeek { get; set; }
        
        // SeasonGuess
        public int SeasonGuessId { get; set; }
        public SeasonGuess SeasonGuess { get; set; }
    }
}