namespace Betnzer.Data.Models.Guess
{
    public class Table
    {
        public int Id { get; set; }

        // SeasonGuess
        public int SeasonGuessId { get; set; }
        public SeasonGuess SeasonGuess { get; set; }

        // Placements
        public string p1 { get; set; }
        public string p2 { get; set; }
        public string p3 { get; set; }
        public string p4 { get; set; }
        public string p5 { get; set; }
        public string p6 { get; set; }
        public string p7 { get; set; }
        public string p8 { get; set; }
        public string p9 { get; set; }
        public string p10 { get; set; }
        public string p11 { get; set; }
        public string p12 { get; set; }
        public string p13 { get; set; }
        public string p14 { get; set; }
        public string p15 { get; set; }
        public string p16 { get; set; }
        public string p17 { get; set; }
        public string p18 { get; set; }
        public string p19 { get; set; }
        public string p20 { get; set; }
        
    }
}