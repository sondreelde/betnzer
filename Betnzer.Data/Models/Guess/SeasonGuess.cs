using System.Collections.Generic;

namespace Betnzer.Data.Models.Guess
{
    public class SeasonGuess
    {
        public int Id { get; set; }

        // Season
        public int SeasonId { get; set; }
        public Season Season { get; set; }

        // Guesses
        public Table Table { get; set; }
        public ICollection<GoalScorers> GoalScorers { get; set; }
        public ICollection<QuestionAnswer> QuestionAnswer { get; set; }
        public TopScorers TopScorers { get; set; }
        public ICollection<MatchGuess> MatchGuesses { get; set; }


        // Guesser
        public int GuesserId { get; set; }
        public Guesser Guesser { get; set; }
        
        
    }
}