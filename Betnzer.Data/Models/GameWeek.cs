using System.Collections.Generic;
using Betnzer.Data.Models.Guess;

namespace Betnzer.Data.Models
{
    public class GameWeek
    {
        public int Id { get; set; }
        public int WeekNumber { get; set; }

        // SeasonGuess
        public int SeasonId { get; set; }
        public Season Season { get; set; }

        // Game
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public char? Result { get; set; }

        public ICollection<GoalScorers> GoalScorers { get; set; }
        public ICollection<MatchGuess> MatchGuesses { get; set; }



    }
}